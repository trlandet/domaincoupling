///////////////////////////////////////////////////////////////////////////////
// Basic variables
// Defined only if not given on the command line as 
//     gmsh -setnumber l2 5 -setnumber h2 3 ...
If (!Exists(h2)) h2 =   1; EndIf
If (!Exists(l2)) l2 =   4; EndIf
If (!Exists(d)) d = 0.3; EndIf
If (!Exists(f)) f =   2; EndIf
If (!Exists(h)) h = 0.1; EndIf
If (!Exists(angle_deg)) angle_deg = 30.0; EndIf

// Secondary variables
s = Sin(-angle_deg*Pi/180)*d;
c = Cos(-angle_deg*Pi/180)*d;
x0 = f*d;
y0 = -s*f*d/2;
Lc1 = h;
Lc2 = h/10;

///////////////////////////////////////////////////////////////////////////////
// Points

// External boundaries
Point(1) = { 0, -h2/2, 0, Lc1};
Point(2) = {l2, -h2/2, 0, Lc1};
Point(3) = {l2,  h2/2, 0, Lc1};
Point(4) = { 0,  h2/2, 0, Lc1};

// Points on the foil
Point(   5) = {x0 +  5.83900000e-04*c +  4.26030000e-03*s, y0 +  5.83900000e-04*s -  4.26030000e-03*c, 0, Lc2};
Point(   6) = {x0 +  0.00000000e+00*c +  0.00000000e+00*s, y0 +  0.00000000e+00*s -  0.00000000e+00*c, 0, Lc2};
Point(   7) = {x0 +  5.83900000e-04*c + -4.26030000e-03*s, y0 +  5.83900000e-04*s - -4.26030000e-03*c, 0, Lc2};
Point(   8) = {x0 +  2.33420000e-03*c + -8.42890000e-03*s, y0 +  2.33420000e-03*s - -8.42890000e-03*c, 0, Lc2};
Point(   9) = {x0 +  5.24680000e-03*c + -1.25011000e-02*s, y0 +  5.24680000e-03*s - -1.25011000e-02*c, 0, Lc2};
Point(  10) = {x0 +  9.31490000e-03*c + -1.64706000e-02*s, y0 +  9.31490000e-03*s - -1.64706000e-02*c, 0, Lc2};
Point(  11) = {x0 +  1.45291000e-02*c + -2.03300000e-02*s, y0 +  1.45291000e-02*s - -2.03300000e-02*c, 0, Lc2};
Point(  12) = {x0 +  2.08771000e-02*c + -2.40706000e-02*s, y0 +  2.08771000e-02*s - -2.40706000e-02*c, 0, Lc2};
Point(  13) = {x0 +  2.83441000e-02*c + -2.76827000e-02*s, y0 +  2.83441000e-02*s - -2.76827000e-02*c, 0, Lc2};
Point(  14) = {x0 +  3.69127000e-02*c + -3.11559000e-02*s, y0 +  3.69127000e-02*s - -3.11559000e-02*c, 0, Lc2};
Point(  15) = {x0 +  4.65628000e-02*c + -3.44792000e-02*s, y0 +  4.65628000e-02*s - -3.44792000e-02*c, 0, Lc2};
Point(  16) = {x0 +  5.72720000e-02*c + -3.76414000e-02*s, y0 +  5.72720000e-02*s - -3.76414000e-02*c, 0, Lc2};
Point(  17) = {x0 +  6.90152000e-02*c + -4.06310000e-02*s, y0 +  6.90152000e-02*s - -4.06310000e-02*c, 0, Lc2};
Point(  18) = {x0 +  8.17649000e-02*c + -4.34371000e-02*s, y0 +  8.17649000e-02*s - -4.34371000e-02*c, 0, Lc2};
Point(  19) = {x0 +  9.54915000e-02*c + -4.60489000e-02*s, y0 +  9.54915000e-02*s - -4.60489000e-02*c, 0, Lc2};
Point(  20) = {x0 +  1.10162800e-01*c + -4.84567000e-02*s, y0 +  1.10162800e-01*s - -4.84567000e-02*c, 0, Lc2};
Point(  21) = {x0 +  1.25744600e-01*c + -5.06513000e-02*s, y0 +  1.25744600e-01*s - -5.06513000e-02*c, 0, Lc2};
Point(  22) = {x0 +  1.42200500e-01*c + -5.26251000e-02*s, y0 +  1.42200500e-01*s - -5.26251000e-02*c, 0, Lc2};
Point(  23) = {x0 +  1.59492100e-01*c + -5.43715000e-02*s, y0 +  1.59492100e-01*s - -5.43715000e-02*c, 0, Lc2};
Point(  24) = {x0 +  1.77578900e-01*c + -5.58856000e-02*s, y0 +  1.77578900e-01*s - -5.58856000e-02*c, 0, Lc2};
Point(  25) = {x0 +  1.96418700e-01*c + -5.71640000e-02*s, y0 +  1.96418700e-01*s - -5.71640000e-02*c, 0, Lc2};
Point(  26) = {x0 +  2.15967600e-01*c + -5.82048000e-02*s, y0 +  2.15967600e-01*s - -5.82048000e-02*c, 0, Lc2};
Point(  27) = {x0 +  2.36179900e-01*c + -5.90081000e-02*s, y0 +  2.36179900e-01*s - -5.90081000e-02*c, 0, Lc2};
Point(  28) = {x0 +  2.57008300e-01*c + -5.95755000e-02*s, y0 +  2.57008300e-01*s - -5.95755000e-02*c, 0, Lc2};
Point(  29) = {x0 +  2.78404200e-01*c + -5.99102000e-02*s, y0 +  2.78404200e-01*s - -5.99102000e-02*c, 0, Lc2};
Point(  30) = {x0 +  3.00317700e-01*c + -6.00172000e-02*s, y0 +  3.00317700e-01*s - -6.00172000e-02*c, 0, Lc2};
Point(  31) = {x0 +  3.22697600e-01*c + -5.99028000e-02*s, y0 +  3.22697600e-01*s - -5.99028000e-02*c, 0, Lc2};
Point(  32) = {x0 +  3.45491500e-01*c + -5.95747000e-02*s, y0 +  3.45491500e-01*s - -5.95747000e-02*c, 0, Lc2};
Point(  33) = {x0 +  3.68646300e-01*c + -5.90419000e-02*s, y0 +  3.68646300e-01*s - -5.90419000e-02*c, 0, Lc2};
Point(  34) = {x0 +  3.92107900e-01*c + -5.83145000e-02*s, y0 +  3.92107900e-01*s - -5.83145000e-02*c, 0, Lc2};
Point(  35) = {x0 +  4.15821500e-01*c + -5.74033000e-02*s, y0 +  4.15821500e-01*s - -5.74033000e-02*c, 0, Lc2};
Point(  36) = {x0 +  4.39731700e-01*c + -5.63200000e-02*s, y0 +  4.39731700e-01*s - -5.63200000e-02*c, 0, Lc2};
Point(  37) = {x0 +  4.63782600e-01*c + -5.50769000e-02*s, y0 +  4.63782600e-01*s - -5.50769000e-02*c, 0, Lc2};
Point(  38) = {x0 +  4.87918100e-01*c + -5.36866000e-02*s, y0 +  4.87918100e-01*s - -5.36866000e-02*c, 0, Lc2};
Point(  39) = {x0 +  5.12081900e-01*c + -5.21620000e-02*s, y0 +  5.12081900e-01*s - -5.21620000e-02*c, 0, Lc2};
Point(  40) = {x0 +  5.36217400e-01*c + -5.05161000e-02*s, y0 +  5.36217400e-01*s - -5.05161000e-02*c, 0, Lc2};
Point(  41) = {x0 +  5.60268300e-01*c + -4.87619000e-02*s, y0 +  5.60268300e-01*s - -4.87619000e-02*c, 0, Lc2};
Point(  42) = {x0 +  5.84178600e-01*c + -4.69124000e-02*s, y0 +  5.84178600e-01*s - -4.69124000e-02*c, 0, Lc2};
Point(  43) = {x0 +  6.07892100e-01*c + -4.49802000e-02*s, y0 +  6.07892100e-01*s - -4.49802000e-02*c, 0, Lc2};
Point(  44) = {x0 +  6.31353700e-01*c + -4.29778000e-02*s, y0 +  6.31353700e-01*s - -4.29778000e-02*c, 0, Lc2};
Point(  45) = {x0 +  6.54508500e-01*c + -4.09174000e-02*s, y0 +  6.54508500e-01*s - -4.09174000e-02*c, 0, Lc2};
Point(  46) = {x0 +  6.77302500e-01*c + -3.88109000e-02*s, y0 +  6.77302500e-01*s - -3.88109000e-02*c, 0, Lc2};
Point(  47) = {x0 +  6.99682300e-01*c + -3.66700000e-02*s, y0 +  6.99682300e-01*s - -3.66700000e-02*c, 0, Lc2};
Point(  48) = {x0 +  7.21595800e-01*c + -3.45058000e-02*s, y0 +  7.21595800e-01*s - -3.45058000e-02*c, 0, Lc2};
Point(  49) = {x0 +  7.42991700e-01*c + -3.23294000e-02*s, y0 +  7.42991700e-01*s - -3.23294000e-02*c, 0, Lc2};
Point(  50) = {x0 +  7.63820200e-01*c + -3.01515000e-02*s, y0 +  7.63820200e-01*s - -3.01515000e-02*c, 0, Lc2};
Point(  51) = {x0 +  7.84032400e-01*c + -2.79828000e-02*s, y0 +  7.84032400e-01*s - -2.79828000e-02*c, 0, Lc2};
Point(  52) = {x0 +  8.03581300e-01*c + -2.58337000e-02*s, y0 +  8.03581300e-01*s - -2.58337000e-02*c, 0, Lc2};
Point(  53) = {x0 +  8.22421100e-01*c + -2.37142000e-02*s, y0 +  8.22421100e-01*s - -2.37142000e-02*c, 0, Lc2};
Point(  54) = {x0 +  8.40507900e-01*c + -2.16347000e-02*s, y0 +  8.40507900e-01*s - -2.16347000e-02*c, 0, Lc2};
Point(  55) = {x0 +  8.57799500e-01*c + -1.96051000e-02*s, y0 +  8.57799500e-01*s - -1.96051000e-02*c, 0, Lc2};
Point(  56) = {x0 +  8.74255400e-01*c + -1.76353000e-02*s, y0 +  8.74255400e-01*s - -1.76353000e-02*c, 0, Lc2};
Point(  57) = {x0 +  8.89837200e-01*c + -1.57351000e-02*s, y0 +  8.89837200e-01*s - -1.57351000e-02*c, 0, Lc2};
Point(  58) = {x0 +  9.04508500e-01*c + -1.39143000e-02*s, y0 +  9.04508500e-01*s - -1.39143000e-02*c, 0, Lc2};
Point(  59) = {x0 +  9.18235100e-01*c + -1.21823000e-02*s, y0 +  9.18235100e-01*s - -1.21823000e-02*c, 0, Lc2};
Point(  60) = {x0 +  9.30984900e-01*c + -1.05485000e-02*s, y0 +  9.30984900e-01*s - -1.05485000e-02*c, 0, Lc2};
Point(  61) = {x0 +  9.42728000e-01*c + -9.02170000e-03*s, y0 +  9.42728000e-01*s - -9.02170000e-03*c, 0, Lc2};
Point(  62) = {x0 +  9.53437200e-01*c + -7.61080000e-03*s, y0 +  9.53437200e-01*s - -7.61080000e-03*c, 0, Lc2};
Point(  63) = {x0 +  9.63087300e-01*c + -6.32380000e-03*s, y0 +  9.63087300e-01*s - -6.32380000e-03*c, 0, Lc2};
Point(  64) = {x0 +  9.71655900e-01*c + -5.16850000e-03*s, y0 +  9.71655900e-01*s - -5.16850000e-03*c, 0, Lc2};
Point(  65) = {x0 +  9.79122900e-01*c + -4.15190000e-03*s, y0 +  9.79122900e-01*s - -4.15190000e-03*c, 0, Lc2};
Point(  66) = {x0 +  9.85470900e-01*c + -3.28040000e-03*s, y0 +  9.85470900e-01*s - -3.28040000e-03*c, 0, Lc2};
Point(  67) = {x0 +  9.90685000e-01*c + -2.55950000e-03*s, y0 +  9.90685000e-01*s - -2.55950000e-03*c, 0, Lc2};
Point(  68) = {x0 +  9.94753200e-01*c + -1.99380000e-03*s, y0 +  9.94753200e-01*s - -1.99380000e-03*c, 0, Lc2};
Point(  69) = {x0 +  9.97665800e-01*c + -1.58700000e-03*s, y0 +  9.97665800e-01*s - -1.58700000e-03*c, 0, Lc2};
Point(  70) = {x0 +  9.99416100e-01*c + -1.34190000e-03*s, y0 +  9.99416100e-01*s - -1.34190000e-03*c, 0, Lc2};
Point(  71) = {x0 +  1.00000000e+00*c + -1.26000000e-03*s, y0 +  1.00000000e+00*s - -1.26000000e-03*c, 0, Lc2};
Point(  72) = {x0 +  1.00000000e+00*c +  1.26000000e-03*s, y0 +  1.00000000e+00*s -  1.26000000e-03*c, 0, Lc2};
Point(  73) = {x0 +  9.99416100e-01*c +  1.34190000e-03*s, y0 +  9.99416100e-01*s -  1.34190000e-03*c, 0, Lc2};
Point(  74) = {x0 +  9.97665800e-01*c +  1.58700000e-03*s, y0 +  9.97665800e-01*s -  1.58700000e-03*c, 0, Lc2};
Point(  75) = {x0 +  9.94753200e-01*c +  1.99380000e-03*s, y0 +  9.94753200e-01*s -  1.99380000e-03*c, 0, Lc2};
Point(  76) = {x0 +  9.90685000e-01*c +  2.55950000e-03*s, y0 +  9.90685000e-01*s -  2.55950000e-03*c, 0, Lc2};
Point(  77) = {x0 +  9.85470900e-01*c +  3.28040000e-03*s, y0 +  9.85470900e-01*s -  3.28040000e-03*c, 0, Lc2};
Point(  78) = {x0 +  9.79122900e-01*c +  4.15190000e-03*s, y0 +  9.79122900e-01*s -  4.15190000e-03*c, 0, Lc2};
Point(  79) = {x0 +  9.71655900e-01*c +  5.16850000e-03*s, y0 +  9.71655900e-01*s -  5.16850000e-03*c, 0, Lc2};
Point(  80) = {x0 +  9.63087300e-01*c +  6.32380000e-03*s, y0 +  9.63087300e-01*s -  6.32380000e-03*c, 0, Lc2};
Point(  81) = {x0 +  9.53437200e-01*c +  7.61080000e-03*s, y0 +  9.53437200e-01*s -  7.61080000e-03*c, 0, Lc2};
Point(  82) = {x0 +  9.42728000e-01*c +  9.02170000e-03*s, y0 +  9.42728000e-01*s -  9.02170000e-03*c, 0, Lc2};
Point(  83) = {x0 +  9.30984900e-01*c +  1.05485000e-02*s, y0 +  9.30984900e-01*s -  1.05485000e-02*c, 0, Lc2};
Point(  84) = {x0 +  9.18235100e-01*c +  1.21823000e-02*s, y0 +  9.18235100e-01*s -  1.21823000e-02*c, 0, Lc2};
Point(  85) = {x0 +  9.04508500e-01*c +  1.39143000e-02*s, y0 +  9.04508500e-01*s -  1.39143000e-02*c, 0, Lc2};
Point(  86) = {x0 +  8.89837200e-01*c +  1.57351000e-02*s, y0 +  8.89837200e-01*s -  1.57351000e-02*c, 0, Lc2};
Point(  87) = {x0 +  8.74255400e-01*c +  1.76353000e-02*s, y0 +  8.74255400e-01*s -  1.76353000e-02*c, 0, Lc2};
Point(  88) = {x0 +  8.57799500e-01*c +  1.96051000e-02*s, y0 +  8.57799500e-01*s -  1.96051000e-02*c, 0, Lc2};
Point(  89) = {x0 +  8.40507900e-01*c +  2.16347000e-02*s, y0 +  8.40507900e-01*s -  2.16347000e-02*c, 0, Lc2};
Point(  90) = {x0 +  8.22421100e-01*c +  2.37142000e-02*s, y0 +  8.22421100e-01*s -  2.37142000e-02*c, 0, Lc2};
Point(  91) = {x0 +  8.03581300e-01*c +  2.58337000e-02*s, y0 +  8.03581300e-01*s -  2.58337000e-02*c, 0, Lc2};
Point(  92) = {x0 +  7.84032400e-01*c +  2.79828000e-02*s, y0 +  7.84032400e-01*s -  2.79828000e-02*c, 0, Lc2};
Point(  93) = {x0 +  7.63820200e-01*c +  3.01515000e-02*s, y0 +  7.63820200e-01*s -  3.01515000e-02*c, 0, Lc2};
Point(  94) = {x0 +  7.42991700e-01*c +  3.23294000e-02*s, y0 +  7.42991700e-01*s -  3.23294000e-02*c, 0, Lc2};
Point(  95) = {x0 +  7.21595800e-01*c +  3.45058000e-02*s, y0 +  7.21595800e-01*s -  3.45058000e-02*c, 0, Lc2};
Point(  96) = {x0 +  6.99682300e-01*c +  3.66700000e-02*s, y0 +  6.99682300e-01*s -  3.66700000e-02*c, 0, Lc2};
Point(  97) = {x0 +  6.77302500e-01*c +  3.88109000e-02*s, y0 +  6.77302500e-01*s -  3.88109000e-02*c, 0, Lc2};
Point(  98) = {x0 +  6.54508500e-01*c +  4.09174000e-02*s, y0 +  6.54508500e-01*s -  4.09174000e-02*c, 0, Lc2};
Point(  99) = {x0 +  6.31353700e-01*c +  4.29778000e-02*s, y0 +  6.31353700e-01*s -  4.29778000e-02*c, 0, Lc2};
Point( 100) = {x0 +  6.07892100e-01*c +  4.49802000e-02*s, y0 +  6.07892100e-01*s -  4.49802000e-02*c, 0, Lc2};
Point( 101) = {x0 +  5.84178600e-01*c +  4.69124000e-02*s, y0 +  5.84178600e-01*s -  4.69124000e-02*c, 0, Lc2};
Point( 102) = {x0 +  5.60268300e-01*c +  4.87619000e-02*s, y0 +  5.60268300e-01*s -  4.87619000e-02*c, 0, Lc2};
Point( 103) = {x0 +  5.36217400e-01*c +  5.05161000e-02*s, y0 +  5.36217400e-01*s -  5.05161000e-02*c, 0, Lc2};
Point( 104) = {x0 +  5.12081900e-01*c +  5.21620000e-02*s, y0 +  5.12081900e-01*s -  5.21620000e-02*c, 0, Lc2};
Point( 105) = {x0 +  4.87918100e-01*c +  5.36866000e-02*s, y0 +  4.87918100e-01*s -  5.36866000e-02*c, 0, Lc2};
Point( 106) = {x0 +  4.63782600e-01*c +  5.50769000e-02*s, y0 +  4.63782600e-01*s -  5.50769000e-02*c, 0, Lc2};
Point( 107) = {x0 +  4.39731700e-01*c +  5.63200000e-02*s, y0 +  4.39731700e-01*s -  5.63200000e-02*c, 0, Lc2};
Point( 108) = {x0 +  4.15821500e-01*c +  5.74033000e-02*s, y0 +  4.15821500e-01*s -  5.74033000e-02*c, 0, Lc2};
Point( 109) = {x0 +  3.92107900e-01*c +  5.83145000e-02*s, y0 +  3.92107900e-01*s -  5.83145000e-02*c, 0, Lc2};
Point( 110) = {x0 +  3.68646300e-01*c +  5.90419000e-02*s, y0 +  3.68646300e-01*s -  5.90419000e-02*c, 0, Lc2};
Point( 111) = {x0 +  3.45491500e-01*c +  5.95747000e-02*s, y0 +  3.45491500e-01*s -  5.95747000e-02*c, 0, Lc2};
Point( 112) = {x0 +  3.22697600e-01*c +  5.99028000e-02*s, y0 +  3.22697600e-01*s -  5.99028000e-02*c, 0, Lc2};
Point( 113) = {x0 +  3.00317700e-01*c +  6.00172000e-02*s, y0 +  3.00317700e-01*s -  6.00172000e-02*c, 0, Lc2};
Point( 114) = {x0 +  2.78404200e-01*c +  5.99102000e-02*s, y0 +  2.78404200e-01*s -  5.99102000e-02*c, 0, Lc2};
Point( 115) = {x0 +  2.57008300e-01*c +  5.95755000e-02*s, y0 +  2.57008300e-01*s -  5.95755000e-02*c, 0, Lc2};
Point( 116) = {x0 +  2.36179900e-01*c +  5.90081000e-02*s, y0 +  2.36179900e-01*s -  5.90081000e-02*c, 0, Lc2};
Point( 117) = {x0 +  2.15967600e-01*c +  5.82048000e-02*s, y0 +  2.15967600e-01*s -  5.82048000e-02*c, 0, Lc2};
Point( 118) = {x0 +  1.96418700e-01*c +  5.71640000e-02*s, y0 +  1.96418700e-01*s -  5.71640000e-02*c, 0, Lc2};
Point( 119) = {x0 +  1.77578900e-01*c +  5.58856000e-02*s, y0 +  1.77578900e-01*s -  5.58856000e-02*c, 0, Lc2};
Point( 120) = {x0 +  1.59492100e-01*c +  5.43715000e-02*s, y0 +  1.59492100e-01*s -  5.43715000e-02*c, 0, Lc2};
Point( 121) = {x0 +  1.42200500e-01*c +  5.26251000e-02*s, y0 +  1.42200500e-01*s -  5.26251000e-02*c, 0, Lc2};
Point( 122) = {x0 +  1.25744600e-01*c +  5.06513000e-02*s, y0 +  1.25744600e-01*s -  5.06513000e-02*c, 0, Lc2};
Point( 123) = {x0 +  1.10162800e-01*c +  4.84567000e-02*s, y0 +  1.10162800e-01*s -  4.84567000e-02*c, 0, Lc2};
Point( 124) = {x0 +  9.54915000e-02*c +  4.60489000e-02*s, y0 +  9.54915000e-02*s -  4.60489000e-02*c, 0, Lc2};
Point( 125) = {x0 +  8.17649000e-02*c +  4.34371000e-02*s, y0 +  8.17649000e-02*s -  4.34371000e-02*c, 0, Lc2};
Point( 126) = {x0 +  6.90152000e-02*c +  4.06310000e-02*s, y0 +  6.90152000e-02*s -  4.06310000e-02*c, 0, Lc2};
Point( 127) = {x0 +  5.72720000e-02*c +  3.76414000e-02*s, y0 +  5.72720000e-02*s -  3.76414000e-02*c, 0, Lc2};
Point( 128) = {x0 +  4.65628000e-02*c +  3.44792000e-02*s, y0 +  4.65628000e-02*s -  3.44792000e-02*c, 0, Lc2};
Point( 129) = {x0 +  3.69127000e-02*c +  3.11559000e-02*s, y0 +  3.69127000e-02*s -  3.11559000e-02*c, 0, Lc2};
Point( 130) = {x0 +  2.83441000e-02*c +  2.76827000e-02*s, y0 +  2.83441000e-02*s -  2.76827000e-02*c, 0, Lc2};
Point( 131) = {x0 +  2.08771000e-02*c +  2.40706000e-02*s, y0 +  2.08771000e-02*s -  2.40706000e-02*c, 0, Lc2};
Point( 132) = {x0 +  1.45291000e-02*c +  2.03300000e-02*s, y0 +  1.45291000e-02*s -  2.03300000e-02*c, 0, Lc2};
Point( 133) = {x0 +  9.31490000e-03*c +  1.64706000e-02*s, y0 +  9.31490000e-03*s -  1.64706000e-02*c, 0, Lc2};
Point( 134) = {x0 +  5.24680000e-03*c +  1.25011000e-02*s, y0 +  5.24680000e-03*s -  1.25011000e-02*c, 0, Lc2};
Point( 135) = {x0 +  2.33420000e-03*c +  8.42890000e-03*s, y0 +  2.33420000e-03*s -  8.42890000e-03*c, 0, Lc2};

///////////////////////////////////////////////////////////////////////////////
// Lines

// Straight lines on the external boundary
Line(1)  = {1,2}; 
Line(2)  = {2,3};
Line(3)  = {3,4};
Line(4)  = {4,1};

// Line segments on the foil
Line(   5)  = {   5,   6};
Line(   6)  = {   6,   7};
Line(   7)  = {   7,   8};
Line(   8)  = {   8,   9};
Line(   9)  = {   9,  10};
Line(  10)  = {  10,  11};
Line(  11)  = {  11,  12};
Line(  12)  = {  12,  13};
Line(  13)  = {  13,  14};
Line(  14)  = {  14,  15};
Line(  15)  = {  15,  16};
Line(  16)  = {  16,  17};
Line(  17)  = {  17,  18};
Line(  18)  = {  18,  19};
Line(  19)  = {  19,  20};
Line(  20)  = {  20,  21};
Line(  21)  = {  21,  22};
Line(  22)  = {  22,  23};
Line(  23)  = {  23,  24};
Line(  24)  = {  24,  25};
Line(  25)  = {  25,  26};
Line(  26)  = {  26,  27};
Line(  27)  = {  27,  28};
Line(  28)  = {  28,  29};
Line(  29)  = {  29,  30};
Line(  30)  = {  30,  31};
Line(  31)  = {  31,  32};
Line(  32)  = {  32,  33};
Line(  33)  = {  33,  34};
Line(  34)  = {  34,  35};
Line(  35)  = {  35,  36};
Line(  36)  = {  36,  37};
Line(  37)  = {  37,  38};
Line(  38)  = {  38,  39};
Line(  39)  = {  39,  40};
Line(  40)  = {  40,  41};
Line(  41)  = {  41,  42};
Line(  42)  = {  42,  43};
Line(  43)  = {  43,  44};
Line(  44)  = {  44,  45};
Line(  45)  = {  45,  46};
Line(  46)  = {  46,  47};
Line(  47)  = {  47,  48};
Line(  48)  = {  48,  49};
Line(  49)  = {  49,  50};
Line(  50)  = {  50,  51};
Line(  51)  = {  51,  52};
Line(  52)  = {  52,  53};
Line(  53)  = {  53,  54};
Line(  54)  = {  54,  55};
Line(  55)  = {  55,  56};
Line(  56)  = {  56,  57};
Line(  57)  = {  57,  58};
Line(  58)  = {  58,  59};
Line(  59)  = {  59,  60};
Line(  60)  = {  60,  61};
Line(  61)  = {  61,  62};
Line(  62)  = {  62,  63};
Line(  63)  = {  63,  64};
Line(  64)  = {  64,  65};
Line(  65)  = {  65,  66};
Line(  66)  = {  66,  67};
Line(  67)  = {  67,  68};
Line(  68)  = {  68,  69};
Line(  69)  = {  69,  70};
Line(  70)  = {  70,  71};
Line(  71)  = {  71,  72};
Line(  72)  = {  72,  73};
Line(  73)  = {  73,  74};
Line(  74)  = {  74,  75};
Line(  75)  = {  75,  76};
Line(  76)  = {  76,  77};
Line(  77)  = {  77,  78};
Line(  78)  = {  78,  79};
Line(  79)  = {  79,  80};
Line(  80)  = {  80,  81};
Line(  81)  = {  81,  82};
Line(  82)  = {  82,  83};
Line(  83)  = {  83,  84};
Line(  84)  = {  84,  85};
Line(  85)  = {  85,  86};
Line(  86)  = {  86,  87};
Line(  87)  = {  87,  88};
Line(  88)  = {  88,  89};
Line(  89)  = {  89,  90};
Line(  90)  = {  90,  91};
Line(  91)  = {  91,  92};
Line(  92)  = {  92,  93};
Line(  93)  = {  93,  94};
Line(  94)  = {  94,  95};
Line(  95)  = {  95,  96};
Line(  96)  = {  96,  97};
Line(  97)  = {  97,  98};
Line(  98)  = {  98,  99};
Line(  99)  = {  99, 100};
Line( 100)  = { 100, 101};
Line( 101)  = { 101, 102};
Line( 102)  = { 102, 103};
Line( 103)  = { 103, 104};
Line( 104)  = { 104, 105};
Line( 105)  = { 105, 106};
Line( 106)  = { 106, 107};
Line( 107)  = { 107, 108};
Line( 108)  = { 108, 109};
Line( 109)  = { 109, 110};
Line( 110)  = { 110, 111};
Line( 111)  = { 111, 112};
Line( 112)  = { 112, 113};
Line( 113)  = { 113, 114};
Line( 114)  = { 114, 115};
Line( 115)  = { 115, 116};
Line( 116)  = { 116, 117};
Line( 117)  = { 117, 118};
Line( 118)  = { 118, 119};
Line( 119)  = { 119, 120};
Line( 120)  = { 120, 121};
Line( 121)  = { 121, 122};
Line( 122)  = { 122, 123};
Line( 123)  = { 123, 124};
Line( 124)  = { 124, 125};
Line( 125)  = { 125, 126};
Line( 126)  = { 126, 127};
Line( 127)  = { 127, 128};
Line( 128)  = { 128, 129};
Line( 129)  = { 129, 130};
Line( 130)  = { 130, 131};
Line( 131)  = { 131, 132};
Line( 132)  = { 132, 133};
Line( 133)  = { 133, 134};
Line( 134)  = { 134, 135};
Line( 135)  = { 135,   5};


///////////////////////////////////////////////////////////////////////////////

// Line loops

Line Loop(1)  = {1,2,3,4};
Line Loop(2) = {5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,
                27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,
                48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,
                69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,
                90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,
                108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,
                123,124,125,126,127,128,129,130,131,132,133,134,135};

// Surfaces, the first is the outer line loop, the following are the hole line loops

Plane Surface(1) = {1,2};

///////////////////////////////////////////////////////////////////////////////
// Physical lines (for use with boundary conditions)

Physical Line(1) = {1};
Physical Line(2) = {2};
Physical Line(3) = {3};
Physical Line(4) = {4};
Physical Line(5) = {5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,
                    27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,
                    48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,
                    69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,
                    90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,
                    108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,
                    123,124,125,126,127,128,129,130,131,132,133,134,135};

// Physical Surface - dolfin convert requires this for some reason

Physical Surface(0) = {1};
