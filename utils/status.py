import os, sys, subprocess
import h5py


def status(inpfile):
    print inpfile,
    h5_file = inpfile[:-12] + 'restart.h5'
    if not os.path.isfile(h5_file):
        print '   <-- missing h5'
        return
    print
    
    with h5py.File(h5_file, 'r') as hdf:
        t = hdf['/reports/t']       
        du = hdf['/reports/du']
        print '    t:  % 15.3f' % t[-1]
        print '    du: % 15.4e' % du[-1]


if __name__ == '__main__':
    roots = sys.argv[1:]
    for root in roots:
        for dirname, dirs, files in os.walk(root):
            dirs.sort()
            first_in_dir = True
            for filename in sorted(files):
                if filename.endswith('.inp'):
                    if first_in_dir:
                        print
                    first_in_dir = False
                    status(os.path.join(dirname, filename))

