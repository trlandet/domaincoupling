"""
Read h5 files made by running the parameter study (run_many.py) and
produce output (evaluate.out) that can be plotted (make_plots.py)
"""
import os, sys
import numpy
import h5py
import dolfin as df


GROUP_FILTERS = 'N10', 'N20', 'N30'
OUTFILE = open('evaluate.out', 'wt')


def calculate_error(h5, h5a):
    """
    Calculate the error between the solution stored in h5 and
    the "correct" solution stored in h5a
    """
    if not h5.has_dataset('metadata'):
        return -1, -1, -1
    
    mesh, mesha = df.Mesh(), df.Mesh()
    h5.read(mesh, '/mesh', False)
    h5a.read(mesha, '/mesh', False)
    
    # Mark the Navier-Stokes domain
    cell_marker = df.CellFunction('size_t', mesh)
    cell_marker.set_all(0)
    class NSDomain(df.SubDomain):
        def inside(self, x, on_boundary):
            return x[0] > - 1e-8
    ns_marker = NSDomain()
    ns_marker.mark(cell_marker, 1)
    dx = df.Measure('dx')(subdomain_data=cell_marker)(1)
    
    errs = []
    bases = []
    for funcname, space, degree in (('u0', 'CG', 2),
                                    ('u1', 'CG', 2),
                                    ('p',  'CG', 1)):
        V = df.FunctionSpace(mesh, space, degree)
        Va = df.FunctionSpace(mesha, space, degree)
        Vi = df.FunctionSpace(mesh, space, degree+3)
        
        f, fa = df.Function(V), df.Function(Va)
        h5.read(f, '/%s' % funcname)
        h5a.read(fa, '/%s' % funcname)
        
        fa.set_allow_extrapolation(True)
        fi = df.project(f, Vi)
        fai = df.project(fa, Vi)
        
        err = df.assemble((fi-fai)**2*dx)
        base = df.assemble(fai**2*dx)
        errs.append(err)
        bases.append(base)
    
    err_u0, err_u1, err_p = errs
    base_u0, base_u1, base_p = bases
    err_u = (err_u0 + err_u1)/(base_u0 + base_u1)
    err_p /= base_p
    return err_u**0.5, err_p**0.5


def get_timeseries(h5_file_name):
    hdf = h5py.File(h5_file_name, 'r')
    timeseries = {}
    
    for tsname in hdf['/reports']:
        timeseries[str(tsname)] = numpy.array(hdf['/reports'][tsname])
    
    return timeseries


def log(message):
    for f in (sys.stdout, OUTFILE):
        f.write(message)


def evaluate_group(groupname, inpfiles):
    log('%s\n' % groupname)
    
    h5_uncoupled = None
    h5_files = []
    for inpfile in inpfiles:
        h5_file_name = inpfile[:-12] + 'restart.h5'
        if not os.path.isfile(h5_file_name):
            continue
        
        hdf = df.HDF5File(df.mpi_comm_world(), h5_file_name, 'r')
        
        if 'uncoupled' in inpfile:
            h5_uncoupled = h5_file_name, hdf
        else:
            h5_files.append((h5_file_name, hdf))
    
    if not h5_uncoupled:
        log('    NO UNCOUPLED\n')
        return
    h5_file_uncoupled, hdf_uncoupled = h5_uncoupled
    
    for h5_file_name, hdf in h5_files:
        err_u, err_p = calculate_error(hdf, hdf_uncoupled)
        hdf.close()
        log('    %-70s: % 15.5e % 15.5e' % (h5_file_name, err_u, err_p))
        
        ts = get_timeseries(h5_file_name)
        F0 = ts['Fp0'][-1] + ts['Fv0'][-1] 
        log('    % 15.5e\n' % (F0,))
    
    hdf_uncoupled.close()
    ts = get_timeseries(h5_file_uncoupled)
    F0 = ts['Fp0'][-1] + ts['Fv0'][-1]
    log('    %-70s:     "answer" %s % 15.5e\n' % (h5_file_uncoupled, ' '*21, F0))


def find_groups(root_dirs):
    groups = {f: [] for f in GROUP_FILTERS}
    def add_inp_file(fn):
        for group in GROUP_FILTERS:
            if group in fn:
                groups[group].append(fn)
    
    for root in roots:
        for dirname, _dirs, files in os.walk(root):
            for filename in files:
                if filename.endswith('.inp'):
                    add_inp_file(os.path.join(dirname, filename))
    
    return groups


if __name__ == '__main__':
    df.set_log_level(df.WARNING)
    roots = sys.argv[1:]
    groups = find_groups(roots)
    for group in sorted(groups):
        evaluate_group(group, groups[group])
