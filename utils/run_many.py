"""
Run as 

    PYTHONPATH=../HarmonicPolynomialCell/:./src/ frun-master python utils/run_many.py

Where the frun-master script makes sure gmsh and FEniCS paths are setup properly
"""


INPFILE = 'input/circle_Re30.inp'
RESDIR = 'results/circle_Re30'
DISCRETISATIONS = (10, 20, 30)
COUPLINGS = ('natural', 'dirichlet', 'mixed')
METHODS = ('femfem_scipy', 'femhpc_scipy', 'femfem_dolfin')
FILTERS = ['mixed', 'scipy']


###############################################################################


import os, subprocess, fnmatch
from utilities import Input


def run_one(N, coupling, method, all_viscous=False):
    inp = Input()
    inp.read(INPFILE)
    
    dirname = '%s/%s_%s' % (RESDIR, method, coupling)
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
    
    prefix = dirname + '/N%d_' % N
    inpname = prefix + 'coupling.inp'
    
    for filt in FILTERS:
        if '*' in filt and not fnmatch.fnmatch(inpname, filt):
            return
        elif filt not in inpname:
            return
    print inpname
    
    inp.N1 = N
    inp.N2 = N
    inp.coupling_method = coupling
    inp.solution_method = method
    inp.all_viscous = all_viscous
    inp.output_prefix = prefix
    inp.output_step = 100
    inp.output_step_fulldump = 100
    
    with open(inpname, 'wt') as f:
        f.write(str(inp))
    
    return # Comment this out to actually enqueue the job
    
    # Enqueue job with Task Spooler (http://vicerveza.homeunix.net/~viric/soft/ts/)
    cmd = ['ts', 'python', 'src/domain_coupling.py', inpname]
    with open('/dev/null', 'w') as devnull:
        #print '   ', ' '.join(cmd)
        subprocess.call(cmd, stdout=devnull, stderr=devnull)
    
    global njobs
    njobs += 1


print '\nSubmitting jobs:\n-----------------------'
njobs = 0

for N in DISCRETISATIONS:
    for coupling in COUPLINGS:
        for method in METHODS:
            run_one(N, coupling, method)
    run_one(N, 'uncoupled', 'femfem_dolfin', all_viscous=True)

print '-----------------------\nSubmitted %d jobs' % njobs
