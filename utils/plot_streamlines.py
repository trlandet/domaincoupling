# encoding: utf8
"""
Read a restart file from femfem_dolfin_natural and plot the mesh and streamlines

Run like
PYTHONPATH=src frun-master python utils/plot_streamlines.py ~/Work/LaTeX/FEniCS16/Re30_femfem_dolfin_natural_N10_restart.h5 

(frun-master is a bash wrapper that sets up the PATH such that FEniCS master builds of dolfin and friends are used)
"""
from __future__ import division
import sys, os
import dolfin as df
from matplotlib import pyplot
from utilities import StreamFunction, Input

# Setup matplotlib
thisdir = os.path.dirname(__file__)
pyplot.style.use(os.path.join(thisdir, 'paper.mplstyle'))


###############################################################################


h5_file_name = sys.argv[1]
print 'Reading restart file', h5_file_name
h5 = df.HDF5File(df.mpi_comm_world(), h5_file_name, 'r')

# Read the mesh and input
mesh = df.Mesh()
h5.read(mesh, '/mesh', False)
inp_str = h5.attributes('/metadata')['input']
inp = Input()
inp.read_string(inp_str, h5_file_name+':/metadata:input')
print str(inp)

# Mark the Navier-Stokes and potential flow domains
cell_marker = df.CellFunction('size_t', mesh)
cell_marker.set_all(2)
class NSDomain(df.SubDomain):
    def inside(self, x, on_boundary):
        return x[0] > - 1e-8
ns_marker = NSDomain()
ns_marker.mark(cell_marker, 1)
dx_ns = df.Measure('dx')(subdomain_data=cell_marker)(1)
dx_pf = df.Measure('dx')(subdomain_data=cell_marker)(2)

# Read the functions
funcs = []
for funcname, space, degree in (('u0', 'CG', 2),
                                ('u1', 'CG', 2),
                                ('phi',  'CG', 3)):
        V = df.FunctionSpace(mesh, space, degree)
        f = df.Function(V)
        h5.read(f, '/%s' % funcname)
        funcs.append(f)
u0, u1, phi = funcs



###############################################################################



def create_combined_functions(u0, u1, phi, dx_ns, dx_pf):
    """
    Define projections from functions defined on the NS and PF domains
    into one global velocity field
    """
    inc_NS = df.Constant(1)
    inc_PF = df.Constant(1)
    
    # Combined velocity, uC
    V = u0.function_space()
    u, v = df.TrialFunction(V), df.TestFunction(V)
    a = u*v*inc_NS*dx_ns + u*v*inc_PF*dx_pf
    L0 = inc_NS*u0*v*dx_ns + inc_PF*phi.dx(0)*v*dx_pf
    L1 = inc_NS*u1*v*dx_ns + inc_PF*phi.dx(1)*v*dx_pf
    A = df.assemble(a)
    A.ident_zeros()
    uC = df.as_vector([df.Function(V), df.Function(V)])
    return uC, A, L0, L1

# Combined velocity
uC, A, L0, L1 = create_combined_functions(u0, u1, phi, dx_ns, dx_pf)
b0 = df.assemble(L0)
b1 = df.assemble(L1)
df.solve(A, uC[0].vector(), b0)
df.solve(A, uC[1].vector(), b1)

# Stream lines of the combined velocity
sf = StreamFunction(uC)
sf.compute()


###############################################################################


# Make the plot
fig = pyplot.figure(figsize=(30, 6))
#ax = fig.add_axes((0.02, 0.02, 0.96, 0.96))
ax = fig.add_axes((0, 0, 1, 1))
coords = mesh.coordinates()
sieve = (coords[:,0] < 1e-6) & (coords[:,1] > -1e-6) 
coords_pf = coords[sieve]
ax.plot(coords_pf[:,0], coords_pf[:,1], 'og', alpha=0.5)
sf.plot(ax, lw=0.5, levels=81, mesh_alpha=0.5, mesh_lw=0.2, cut=None)#(1, -0.1, True))

ax.axis('off')
ax.axis('equal')

infotext = dict(horizontalalignment='left', verticalalignment='top',
                fontsize=20, backgroundcolor='white')
ax.text(-inp.l1+inp.l1/20, inp.h1/2.1, 'Potential flow subdomain', **infotext)
ax.text(0+inp.l1/20, inp.h2/2.1, 'Navier-Stokes subdomain',  **infotext)
ax.text(0+inp.l1/100, 1.5, '$\leftarrow$Coupling interface',  **infotext)


fig.savefig('streamlines.pdf')
fig.savefig('streamlines_raster2.png', dpi=300)
pyplot.show()

