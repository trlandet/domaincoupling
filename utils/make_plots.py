"""
Read evaluate.out file made by postprocessing the parameter study (evaluate.py)
and produce plots
"""
from __future__ import division
import os
from matplotlib import pyplot

###############################################################################
# Plot styling

thisdir = os.path.dirname(__file__)
pyplot.style.use(os.path.join(thisdir, 'paper.mplstyle'))

def name_for_method(method):
    m1 = 'HPC' if 'hpc' in method else 'FEM'
    i = method[7:].index('_')
    m2 = method[8+i:]
    return r'$\mathrm{%s\ %s}$' % (m1, m2.title())

def args_for_method(method):
    a = {}
    a['label'] = name_for_method(method)
    a['linewidth'] = 2.0
    if 'hpc' in method:
        a['color'] = '#797cde' # Light blue
        a['marker'] = 's'
        a['markersize'] = 3
        a['markerfacecolor'] = 'white'
        a['markeredgecolor'] = a['color']
        a['markeredgewidth'] = 1.5
        
    else:
        a['color'] = '#26563f' # Dark green
        #a['marker'] = 'x'
    
    if 'natural' in method:
        a['linestyle'] = '--'
    elif 'mixed' in method:
        a['linestyle'] = '-.'
    else:
        a['linestyle'] = ':'
    
    if 'ucuc' in method:
        a['color'] = '#22858f' # mid turquoise
    
    return a


###############################################################################

# Read evaluate.out
evaluate_results = {}
for line in open('evaluate.out', 'rt'):
    if not ':' in line:
        continue
    
    pth, res = line.split(':')
    pth1, fn = os.path.split(pth)
    pth2, dn = os.path.split(pth1)
    N = int(fn.split('_')[0][1:])
    
    if not N in evaluate_results:
        evaluate_results[N] = {}
    
    if not 'answer' in line:
        r0, r1, f0 = [float(v) for v in res.split()]
        evaluate_results[N][dn] = (r0, r1, f0)
    else:
        wds = res.split()
        ans_f0 = float(wds[1])

Ns = sorted(evaluate_results)
methods = set()
for N in Ns:
    methods.update(evaluate_results[N].keys())
methods = sorted(methods)
methods = [m for m in methods if 'dolfin' not in m and 'mixed' not in m]
#methods = [m for m in methods if 'hpc' not in m]

def get(method, N, i):
    d = evaluate_results[N]
    if method in d:
        return d[method][i]
    else:
        return None

# Plot error in u
#fig = pyplot.figure(0, figsize=(6,10))
#pyplot.subplot(311)
pyplot.figure(0, figsize=(8,4))
pyplot.title(r'$\mathrm{L_2\ error\ in\ velocity}$')
for method in methods:
    errs = [get(method, N, 0) for N in Ns]
    n2, e2 = zip(*[(N, e) for N, e in zip(Ns, errs) if e is not None]) 
    pyplot.loglog(n2, e2, **args_for_method(method))
pyplot.ylabel(r'$||E_\mathbf{u}||$')


# Plot error in p
pyplot.figure(1, figsize=(8,4))
#pyplot.subplot(312)
pyplot.title(r'$\mathrm{L_2\ error\ in\ pressure}$')
for method in methods:
    errs = [get(method, N, 1) for N in Ns]
    n2, e2 = zip(*[(N, e) for N, e in zip(Ns, errs) if e is not None]) 
    pyplot.loglog(n2, e2, **args_for_method(method))
pyplot.ylabel(r'$||E_p||$')


# Plot drag force
pyplot.figure(2, figsize=(8,4))
#pyplot.subplot(313)
pyplot.title(r'$\mathrm{Drag\ force}$')
pyplot.axhline(ans_f0, ls='-', c='#444444', lw=0.8)
for method in methods:
    errs = [get(method, N, 2) for N in Ns]
    n2, e2 = zip(*[(N, e) for N, e in zip(Ns, errs) if e is not None]) 
    pyplot.plot(n2, e2, **args_for_method(method))
pyplot.ylabel(r'$F_D$')


for fig_idx in (0, 1, 2):
    fig = pyplot.figure(fig_idx)
    ax = pyplot.gca()
    #ax = pyplot.subplot(3, 1, fig_idx+1)
    pyplot.xlabel('$N$')
    pyplot.xticks(Ns, [str(N) for N in Ns])
    
    if fig_idx == 0:
        pyplot.axis([9, 31, 0.7e-3, 1e-2])
    elif fig_idx == 1:
        pyplot.axis([9, 31, 0.7e-3, 3e-2])
    elif fig_idx == 2:
        #pyplot.axis([9, 31, 1.1, 1.2])
        pyplot.axis([9, 31, 1.12, 1.16])
    
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    
    pyplot.legend(loc='best')
    fig.tight_layout()

pyplot.show()

