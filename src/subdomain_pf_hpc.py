# encoding: utf8
from __future__ import division
import numpy
import hpc
from utilities import COUPLED_NATURAL


class PotentialHPCSubDomain(object):
    def __init__(self, inp):
        assert inp.layout == 'I'
        hpc.parameters['linear_algebra_backend'] = 'scipy'
        self.input = inp
        self.neumann_outlet = inp.coupling_method == COUPLED_NATURAL 
        
        p0 = (-inp.l1, -inp.h1/2)
        p1 = (0, inp.h1/2)
        Ny = self.input.N1
        Nx = int(round(Ny*self.input.l1/self.input.h1))
        
        self.domain = hpc.rectangle_domain(p0, p1, Nx, Ny)
        self.phi = numpy.zeros(len(self.domain.dof_coordinates), float)
        self.phi_p = numpy.zeros_like(self.phi)
        self.phi_pp = numpy.zeros_like(self.phi)
        self._tensor_cache = None
        self._bc_cache = None
        self.is_first_time_step = True
    
    def get_dividing_line(self, line_number):
        """
        Get the dofs on the given line between the Navier-Stokes and potential 
        flow domains
        
          +---------------------------------
          |          
          |     +---line 1------------------
          |     |     
          |     |<--line 0
          |     |
          |     +---line 2------------------
          | 
          +---------------------------------
        
        The coordinate system is such that x = 0 on line 0 and y=+/- h2/2 on
        lines 1 and 2
        """
        assert line_number == 0
        
        dividing_line = []
        for dof, coord in enumerate(self.domain.dof_coordinates):
            if coord[0] > -1e-8:
                dividing_line.append((dof, coord))
        
        dividing_line.sort(key=lambda item: item[1][0]+item[1][1])
        return dividing_line
    
    def get_gradient_weights(self, coord, dof0, dof1):
        """
        Return the dofs and weights needed to calculate the gradient of phi at
        a given coordinate positioned somewhere on the line between the
        locations of dof0 and dof1.
        
        The dofs and weights for the x-derivative are dofs, coeffs[:,0] and
        the y-derivatives can be evaluated from dofs, coeffs[:,1].
        
        The method returns dofs, coeffs
        """
        domain = self.domain
        coord0 = domain.dof_coordinates[dof0]
        coord1 = domain.dof_coordinates[dof1]
        
        d0 = ((coord0[0] - coord[0])**2 + (coord0[1] - coord[1])**2)**0.5
        d1 = ((coord1[0] - coord[0])**2 + (coord1[1] - coord[1])**2)**0.5
        fac = d1/(d0 + d1)
        
        nbs0, _, cx0, cy0 = hpc.eval_phi(domain, dof0)
        nbs1, _, cx1, cy1 = hpc.eval_phi(domain, dof1)
        
        dofs = numpy.zeros(16, int)
        coeffs = numpy.zeros((16, 2), float)
        dofs[:8] = nbs0
        dofs[8:] = nbs1
        coeffs[:8,0] = fac*cx0
        coeffs[8:,0] = (1-fac)*cx1
        coeffs[:8,1] = fac*cy0
        coeffs[8:,1] = (1-fac)*cy1
        
        return dofs, coeffs
    
    def get_all_weights(self, coord, dof0, dof1):
        """
        Return the dofs and weights needed to calculate the phi, the gradient of
        phi and the double gradient of phi at a given coordinate positioned
        somewhere on the line between the locations of dof0 and dof1.
        
        This method returns the dofs and the coefficients where the i-th column
        of the coefficient matrix is as follows:
        
          - coeffs[:,0] = weights for ϕ
          - coeffs[:,1] = weights for ∂ϕ/∂x
          - coeffs[:,2] = weights for ∂ϕ/∂y 
          - coeffs[:,3] = weights for ∂²ϕ/∂x∂x
          - coeffs[:,4] = weights for ∂²ϕ/∂x∂y
          - coeffs[:,5] = weights for ∂²ϕ/∂y∂x
          - coeffs[:,6] = weights for ∂²ϕ/∂y∂y
        
        The first dimension of coeffs is the same length as the number of dofs
        """
        domain = self.domain
        coord0 = domain.dof_coordinates[dof0]
        coord1 = domain.dof_coordinates[dof1]
        
        d0 = ((coord0[0] - coord[0])**2 + (coord0[1] - coord[1])**2)**0.5
        d1 = ((coord1[0] - coord[0])**2 + (coord1[1] - coord[1])**2)**0.5
        fac = d1/(d0 + d1)
        
        nbs0, c0, cx0, cy0, cxx0, cxy0, cyx0, cyy0 = hpc.eval_phi(domain, dof0, grad_grad=True)
        nbs1, c1, cx1, cy1, cxx1, cxy1, cyx1, cyy1 = hpc.eval_phi(domain, dof1, grad_grad=True)
        
        dofs = numpy.zeros(16, int)
        coeffs = numpy.zeros((16, 7), float)
        dofs[:8] = nbs0
        dofs[8:] = nbs1
        coeffs[:8,0] = fac*c0;      coeffs[8:,0] = (1-fac)*c1
        coeffs[:8,1] = fac*cx0;     coeffs[8:,1] = (1-fac)*cx1
        coeffs[:8,2] = fac*cy0;     coeffs[8:,2] = (1-fac)*cy1
        coeffs[:8,3] = fac*cxx0;    coeffs[8:,3] = (1-fac)*cxx1
        coeffs[:8,4] = fac*cxy0;    coeffs[8:,4] = (1-fac)*cxy1
        coeffs[:8,5] = fac*cyx0;    coeffs[8:,5] = (1-fac)*cyx1
        coeffs[:8,6] = fac*cyy0;    coeffs[8:,6] = (1-fac)*cyy1
        
        return dofs, coeffs
        
    
    def velocity_dofs_and_weights(self, dof):
        nbs, _, cdx, cdy = hpc.eval_phi(self.domain, dof)
        N = len(nbs)
        coeffs = numpy.zeros((N, 2), float)
        coeffs[:,0] = cdx
        coeffs[:,1] = cdy
        return nbs, coeffs
    
    def get_system(self, t):
        """
        Return linear system with normal BCs applied (not coupled)
        Matrix format is SciPy CSR
        """
        # The matrices are time invariant
        if self._tensor_cache is None:
            A, b = hpc.assemble(self.domain)
            A, b = A.csr_matrix, b.array()
            self._tensor_cache = A, b
        else:
            self.is_first_time_step = False
            A, b = self._tensor_cache
        
        # Apply boundary conditions (time dependent)
        U0 = self.input.inlet_vel(t)
        if self._bc_cache is None:
            bcs, inlet_dofs = self._get_boundary_conditions(t)
            hpc.apply_bcs(self.domain, A, b, bcs)
            self._bc_cache = numpy.array(inlet_dofs, int)
        else:
            inlet_dofs = self._bc_cache
            b[inlet_dofs] = U0
        
        return A, b
    
    def explicit_velocity_at_dof(self, dof, prev=0):
        if prev == 0:
            vec = self.phi
        elif prev == 'conv':
            if self.is_first_time_step:
                vec = self.phi_p
            else:
                vec = 2*self.phi_p - self.phi_pp
        elif prev == 1:
            vec = self.phi_p
        else:
            assert prev == 2
            vec = self.phi_pp
        
        neighbours, _coeffs, coeffsdx, coeffsdy = hpc.eval_phi(self.domain, dof)
        u = v = 0
        for nb, wu, wv in zip(neighbours, coeffsdx, coeffsdy):
            phi = vec[nb]
            u += wu*phi
            v += wv*phi
        return u, v
    
    def update(self, phi):
        self.phi_pp[:] = self.phi_p
        self.phi_p[:] = self.phi
        self.phi[:] = phi
    
    def get_triangulation(self):
        coords = self.domain.dof_coordinates
        triangles = self.domain.triangles
        return coords, triangles
    
    def get_arrays(self):
        return self.phi, self.phi_p
    
    def get_data(self, func_name):
        N = len(self.domain.dof_coordinates)
        values = numpy.zeros(N, float)
        rho = self.input.rho
        dt = self.input.dt
        
        if self.is_first_time_step:
            tc0, tc1, tc2 = [1.0/dt, -1.0/dt, 0.0]
        else:
            tc0, tc1, tc2 = [1.5/dt, -2.0/dt, 0.5/dt]
        
        for dof in range(N):
            vel = self.explicit_velocity_at_dof(dof)
            if func_name == 'u0':
                values[dof] = vel[0]
            elif func_name == 'u1':
                values[dof] = vel[1]
            elif func_name == 'p':
                vel_c = self.explicit_velocity_at_dof(dof, 'conv')
                vel2 = vel[0]*vel_c[0] + vel[1]*vel_c[1]
                dphi_dt = tc0*self.phi[dof] + tc1*self.phi_p[dof] + tc2*self.phi_pp[dof]
                values[dof] = -vel2*rho/2 - dphi_dt*rho
        
        if func_name == 'p':
            values *= rho
        
        return values
    
    def _get_boundary_conditions(self, U0):
        inp, domain = self.input, self.domain
        
        bcs = []
        inlet_dofs = []
        for dof, coord in enumerate(domain.dof_coordinates): 
            if domain.dof_type[dof] == hpc.DOF_TYPE_EXTERNAL:
                x, y = coord
                if x > -1e-8:
                    if self.neumann_outlet:
                        bcs.append(('Nx', dof, 0))
                    else:
                        # This dof is coupled to N-S - this BC will be overwritten
                        bcs.append(('D', dof, 42))
                elif x < -inp.l1 + 1e-8:
                    bcs.append(('Nx', dof, U0))
                    inlet_dofs.append(dof)
                elif y > inp.h1/2 - 1e-8:
                    bcs.append(('Ny', dof, 0.0))
                elif y < -inp.h1/2 + 1e-8:
                    bcs.append(('Ny', dof, 0.0))
                else:
                    raise 'this should not happen!'
        
        return bcs, inlet_dofs
