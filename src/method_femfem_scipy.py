# encoding: utf-8
from __future__ import division
import scipy.sparse.linalg
from utilities import METHOD_FEMFEM_SP, Coupler
from subdomain_ns_fem import NavierStokesFEMSubDomain
from subdomain_pf_fem import PotentialFEMSubDomain


class FEMFEMScipyMethod(object):
    def __init__(self, inp):
        """
        Method where we create one Navier-Stokes FEM matrix and one
        potential flow FEM matrix and couple them through off diagonal
        coupling blocks in a block matrix defined as
        
            A1 C1 
            C2 A2 
        
        where A1 and A2 are the Navier-Stokes and potential theory blocks
        respectively and C1 and C2 are the "manually" assembled coupling
        blocks (code for this below)
        """
        assert inp.solution_method == METHOD_FEMFEM_SP
        self.input = inp
        
        self.ns_domain = NavierStokesFEMSubDomain(inp)
        self.dx_ns = self.ns_domain.dx
        self.pf_domain = PotentialFEMSubDomain(inp)
        self.coupler = Coupler(inp, self.ns_domain, self.pf_domain)
    
    def assemble(self, t):
        A1, b1 = self.ns_domain.get_system(t)
        A2, b2 = self.pf_domain.get_system(t)
        linear_systems = (A1, A2, b1, b2)
        return linear_systems
    
    def couple(self, linear_systems):
        AA, bb, N1 = self.coupler.couple(linear_systems)
        self.N1 = N1
        return AA, bb
    
    def solve(self, A, b):
        uu = scipy.sparse.linalg.spsolve(A, b)
        
        # Update the solutions in the two sub-domains
        self.ns_domain.update(uu[:self.N1])
        self.pf_domain.update(uu[self.N1:])
    
    def calculate_combined_functions(self):
        # For now we cheat and return only the N-S part 
        return self.ns_domain.u, self.ns_domain.p
    
    def get_force(self):
        return self.ns_domain.get_force()
    
    def plot(self, simulation_time, stream_function=None, quiver=False):
        """
        Plot the combined results in terms of velocity and pressure
        """
        return self.coupler.plot(simulation_time, stream_function, quiver)
