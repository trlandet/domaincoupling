# encoding: utf8
from __future__ import division
import sys, os, time, contextlib, collections, ConfigParser, StringIO
import numpy
import scipy.sparse
import dolfin as df


COUPLED_NO = 'uncoupled'
COUPLED_DIRICHLET = 'dirichlet'
COUPLED_NATURAL = 'natural'
COUPLED_NATURAL_U_DIRICHLET_P = 'mixed'

METHOD_FEMFEM_DF = 'femfem_dolfin'
METHOD_FEMFEM_SP = 'femfem_scipy'
METHOD_FEMHPC_SP = 'femhpc_scipy'

RED = '\033[91m%s\033[0m'    # ANSI escape code Bright Red
YELLOW = '\033[91m%s\033[0m' # ANSI escape code Bright Yellow


class Input(object):
    l1 = 1        # Length before NS domain starts
    l2 = 4        # Length of NS domain
    h1 = 1        # Height of pot domain
    h2 = 1        # Height of NS domain 
    d = 0.1       # Cylinder diameter
    f = 1.5       # Diameters between cylinder center and inlet 
    N1 = N2 = 10  # Geometric discretisation
    
    # Which problem to solve and what type of mesh layout to make
    problem = 'Cylinder'
    geometry = 'circle'
    layout = 'I'
    solution_method = METHOD_FEMFEM_DF
    coupling_method = COUPLED_DIRICHLET # or neumann or uncoupled
    all_viscous = False
    
    U0 = 0.1    # Speed at inlet
    rho = 1     # Density
    Re = 100    # Reynolds number (determines the viscosity)
    
    dt = 0.01   # Timestep
    tmax = 150  # Time duration of the simulation
    tramp = 0.3 # Time duration of the initial inlet velocity ramp-up
    disturbance_time = (10, 12, 14)
    
    output_prefix = 'results/'
    output_step = 1e100
    output_step_fulldump = 1e100 
    
    # Finite element discretization
    Pu = 2
    Pp = 1
    pressure_lagrange_multiplier = False
    use_supg = True
    
    def inlet_vel(self, t):
        fac = 1
        if t < self.tramp:
            fac = 0.5 - 0.5*numpy.cos(numpy.pi*t/self.tramp)
        return self.U0*fac
    
    @property
    def mu(self):
        return self.d*self.U0*self.rho/self.Re

    def disturbance(self, t):
        """
        Disturbance to trigger alternating vortex shedding
        """
        t1, t2, t3 = self.disturbance_time
        if t1 < t < t2:
            tfac = (t - t1)/(t2 - t1)
        elif t2 <= t < t3:
            tfac = 1 - (t - t2)/(t3 - t2)
        else:
            tfac = 0
        return tfac * 0.1 * self.U0
    
    def read(self, inp_file_name):
        """
        Read an input file on INI format with a [DDInput] section
        """
        assert os.path.isfile(inp_file_name)
        cp = ConfigParser.RawConfigParser()
        cp.optionxform = str
        cp.read([inp_file_name])
        self.read_config_parser(cp)
    
    def read_string(self, s, file_name='<string>'):
        cp = ConfigParser.RawConfigParser()
        cp.optionxform = str
        fp = StringIO.StringIO(s)
        cp.readfp(fp, '<string>')
        self.read_config_parser(cp)
    
    def read_config_parser(self, cp):
        SECTION = 'DDInput'
        assert cp.has_section(SECTION)
        for name in cp.options(SECTION):
            assert hasattr(self, name), 'Input %r was given but is not a valid parameter' % name
            value = eval(cp.get(SECTION, name))
            setattr(self, name, value)
    
    def __str__(self):
        SECTION = 'DDInput'
        cp = ConfigParser.RawConfigParser()
        cp.optionxform = str
        cp.add_section(SECTION)
        for name in sorted(dir(self)):
            if name[0] == '_':
                continue
            tvalue = getattr(type(self), name)
            value = getattr(self, name)
            if hasattr(value, '__call__') or isinstance(tvalue, property):
                continue
            cp.set(SECTION, name, value)
        fp = StringIO.StringIO()
        cp.write(fp)
        fp.seek(0)
        return fp.read()


class SimpleLog(object):
    def __init__(self, logfile=None, console=True):
        """
        SimpleLog lets you print messages to a file and to the 
        console at the same time. Errors will be written to
        console in red and warnings in yellow. The text "ERROR: "
        and "WARNING: " will be prepended to each line of the
        respective type of messages. 
        """
        self.files = []
        if logfile:
            self.files.append(open(logfile, 'wt'))
        if console:
            self.files.append(sys.stdout)
        self.reports = collections.OrderedDict()
        self.report_formats = {}
        self._the_log = []
            
    def info(self, message):
        for f in self.files:
            f.write(message)
        self._the_log.append(message)
    
    def warning(self, message):
        message_warn = message[:-1].replace('\n', '\nWARNING: ')
        message = 'WARNING: %s%s' % (message_warn, message[-1])
        for f in self.files:
            if f.fileno() == 1:
                f.write(YELLOW % message)
            else:
                f.write(message)
        self._the_log.append(message)
    
    def error(self, message):
        message_err = message[:-1].replace('\n', '\nERROR: ')
        message = 'ERROR: %s%s' % (message_err, message[-1])
        for f in self.files:
            if f.fileno() == 1:
                f.write(RED % message)
            else:
                f.write(message)
        self._the_log.append(message)
    
    def dump_object(self, obj, prefix=''):
        for name in sorted(dir(obj)):
            if name[0] == '_':
                continue
            tvalue = getattr(type(obj), name)
            value = getattr(obj, name)
            if hasattr(value, '__call__') or isinstance(tvalue, property):
                continue
            self.info('%s%s = %r\n' % (prefix, name, value))
    
    def flush(self):
        for f in self.files:
            if f.fileno() != 1:
                f.flush()
    
    def get_full_log(self):
        return ''.join(self._the_log)
    
    def report(self, rep_name, value, fmt='% .3e'):
        if not rep_name in self.reports:
            self.reports[rep_name] = []
        self.report_formats[rep_name] = fmt
        self.reports[rep_name].append(value)
    
    def report_timestep(self):
        tsrep = []
        for rep_name, values in self.reports.items():
            fmt = self.report_formats[rep_name]
            if fmt:
                tsrep.append(('%s: ' + fmt) % (rep_name, values[-1]))
        self.info('  '.join(tsrep) + '\n')
    
    @contextlib.contextmanager
    def timer(self, name):
        t_start = time.time()
        yield
        duration = time.time() - t_start
        self.report(name, duration, '%4.2fs')


def mat_to_csr(dolfin_matrix):
    """
    Convert any dolfin.Matrix to csr matrix in scipy.
    Based on code by Miroslav Kuchta
    """
    assert df.MPI.size(df.mpi_comm_world()) == 1, 'mat_to_csr assumes single process'
    
    rows = [0]
    cols = []
    values = []
    for irow in range(dolfin_matrix.size(0)):
        indices, values_ = dolfin_matrix.getrow(irow)
        rows.append(len(indices)+rows[-1])
        cols.extend(indices)
        values.extend(values_)

    shape = dolfin_matrix.size(0), dolfin_matrix.size(1)
        
    return scipy.sparse.csr_matrix((numpy.array(values, dtype='float'),
                                    numpy.array(cols, dtype='int'),
                                    numpy.array(rows, dtype='int')),
                                    shape)


class StreamFunction(object):
    def __init__(self, u, boundary_is_streamline=False, degree=1):
        """
        Heavily based on
        https://github.com/mikaem/fenicstools/blob/master/fenicstools/Streamfunctions.py
        
        Stream function for a given general 2D velocity field.
        The boundary conditions are weakly imposed through the term
        
            inner(q, grad(psi)*n)*ds, 
        
        where grad(psi) = [-v, u] is set on all boundaries. 
        This should work for any collection of boundaries: 
        walls, inlets, outlets etc.    
        """
        Vu = u[0].function_space()
        mesh = Vu.mesh()
        
        # Check dimension
        if not mesh.geometry().dim() == 2:
            df.error("Stream-function can only be computed in 2D.")
    
        # Define the weak form 
        V = df.FunctionSpace(mesh, 'CG', degree)
        q = df.TestFunction(V)
        psi = df.TrialFunction(V)
        n = df.FacetNormal(mesh)
        a = df.dot(df.grad(q), df.grad(psi))*df.dx
        L = df.dot(q, df.curl(u))*df.dx 
        
        if boundary_is_streamline: 
            # Strongly set psi = 0 on entire domain boundary
            self.bcs = [df.DirichletBC(V, df.Constant(0), df.DomainBoundary())]
            self.normalize = False
        else:
            self.bcs = []
            self.normalize = True
            L = L + q*(n[1]*u[0] - n[0]*u[1])*df.ds
            
        # Create preconditioned iterative solver
        solver = df.PETScKrylovSolver('gmres', 'hypre_amg')
        solver.parameters['nonzero_initial_guess'] = True
        solver.parameters['relative_tolerance'] = 1e-10
        solver.parameters['absolute_tolerance'] = 1e-10
        solver.parameters['preconditioner']['structure'] = 'same'
        
        # Store for later computation
        self.psi = df.Function(V)
        self.A = df.assemble(a)
        self.L = L
        self.mesh = mesh
        self.solver = solver
        self._triangulation = None
    
    def compute(self):
        """
        Compute the stream function
        """
        b = df.assemble(self.L)
        
        if self.normalize:
            df.normalize(b)
        
        for bc in self.bcs:
            bc.apply(self.A, b)
        self.solver.solve(self.A, self.psi.vector(), b)
        
        if self.normalize: 
            df.normalize(self.psi.vector())
    
        return self.psi
    
    def plot(self, mpl_ax, levels=50, lw=0.3, mesh_alpha=0, mesh_lw=0.2, cut=None):
        """
        Plot the function on a matplotlib axes. Call .compute() first
        to calculate the stream function
        """
        if self._triangulation is None:
            from matplotlib.tri import Triangulation
            coords = self.mesh.coordinates()
            triangles = []
            for cell in df.cells(self.mesh):
                cell_vertices = cell.entities(0)
                triangles.append(cell_vertices)
            self._triangulation = Triangulation(coords[:,0], coords[:,1], triangles)

        if mesh_alpha > 0:
            mpl_ax.triplot(self._triangulation, color='#000000', alpha=mesh_alpha, lw=mesh_lw)
        
        Z = self.psi.compute_vertex_values()
        if all(Z == 0):
            return
        
        mask = None
        if cut is not None:
            direction, value, above = cut
            # Mask off selected triangles.
            pos = coords[:,direction]
            midpoints = pos[self._triangulation.triangles].mean(axis=1)
            if above:
                mask = numpy.where(midpoints > value, 1, 0)
            else:
                mask = numpy.where(midpoints < value, 1, 0)
        self._triangulation.set_mask(mask)
        
        mpl_ax.tricontour(self._triangulation, Z, levels, colors='#0000AA',
                          linewidths=lw, linestyles='solid')


class SolutionProperties(object):
    def __init__(self, u, dt, nu, dx=None):
        """
        Calculate Courant and Peclet numbers
        """
        self.dx = dx or df.dx
        if dt != 0:
            self._setup_courant(u, dt)
        if nu != 0:
            self._setup_peclet(u, nu)
    
    def _setup_courant(self, vel, dt):
        """
        Co = a*dt/h where a = mag(vel)
        """
        dx = self.dx
        mesh = vel[0].function_space().mesh()
        
        V = df.FunctionSpace(mesh, 'DG', 0)
        h = df.CellSize(mesh)
        u, v = df.TrialFunction(V), df.TestFunction(V)
        a = u*v*dx
        vmag = df.sqrt(df.dot(vel, vel))
        L = vmag*dt/h*v*dx
        
        # Pre-factorize matrices and store for usage in projection
        self._courant_solver = df.LocalSolver(a, L)
        self._courant_solver.factorize()
        self._courant = df.Function(V)
    
    def _setup_peclet(self, vel, nu):
        """
        Pe = a*h/(2*nu) where a = mag(vel)
        """
        dx = self.dx
        mesh = vel[0].function_space().mesh()
        
        V = df.FunctionSpace(mesh, 'DG', 0)
        h = df.CellSize(mesh)
        u, v = df.TrialFunction(V), df.TestFunction(V)
        a = u*v*dx
        L = df.dot(vel, vel)**0.5*h/(2*nu)*v*dx
        
        # Pre-factorize matrices and store for usage in projection
        self._peclet_solver = df.LocalSolver(a, L)
        self._peclet_solver.factorize()
        self._peclet = df.Function(V)
    
    def courant_number(self):
        """
        Calculate the Courant numbers in each cell
        """
        self._courant_solver.solve_local_rhs(self._courant)
        return self._courant
    
    def peclet_number(self):
        """
        Calculate the Peclet numbers in each cell
        """
        self._peclet_solver.solve_local_rhs(self._peclet)
        return self._peclet


def Coupler(inp, ns_domain, pf_domain):
    """
    Return a class that handles coupling of Navier-Stokes and potential flow 
    matrices into one big SciPy block matrix
    """
    if inp.coupling_method in (COUPLED_NATURAL, COUPLED_NATURAL_U_DIRICHLET_P):
        from coupling_natural import NaturalCoupler
        return NaturalCoupler(inp, ns_domain, pf_domain)
    else:
        from coupling_dirichlet import DirichletCoupler
        return DirichletCoupler(inp, ns_domain, pf_domain)


def define_penalty(mesh, P, k_min, k_max, boost_factor=3, exponent=1):
    """
    Define the penalty parameter used in the Poisson equations
    
    Arguments:
        mesh: the mesh used in the simulation
        P: the polynomial degree of the unknown
        k_min: the minimum diffusion coefficient
        k_max: the maximum diffusion coefficient
        boost_factor: the penalty is multiplied by this factor
        exponent: set this to greater than 1 for superpenalisation
    """
    assert k_max >= k_min
    ndim = mesh.geometry().dim()
    
    # Calculate geometrical factor used in the penalty
    geom_fac = 0
    for cell in df.cells(mesh):
        vol = cell.volume()
        area = sum(cell.facet_area(i) for i in range(ndim + 1))
        gf = area/vol
        geom_fac = max(geom_fac, gf)
    geom_fac = df.MPI.max(df.mpi_comm_world(), float(geom_fac))
    
    penalty = boost_factor * k_max**2/k_min * (P + 1)*(P + ndim)/ndim * geom_fac**exponent
    return penalty


def save_simulation(method, h5_file_name, t, it, inp, log):
    """
    Save the domain data to the given h5 file.
    The file will be overwritten if it exists
    """
    if os.path.isfile(h5_file_name):
        os.remove(h5_file_name)
    h5 = df.HDF5File(df.mpi_comm_world(), h5_file_name, 'w')
    
    ns_domain = method.ns_domain
    pf_domain = method.pf_domain
    
    # Write mesh
    h5.write(ns_domain.mesh, '/mesh')
    h5.write(ns_domain.facet_marker, '/facet_marker')
    if inp.solution_method == METHOD_FEMFEM_DF:
        h5.write(ns_domain.cell_marker, '/cell_marker')
    
    # Write FEniCS functions
    candidates = 'u0 u1 u0_p u1_p p p_p phi phi_p'.split()
    funcnames = []
    for name in candidates:
        if hasattr(ns_domain, name):
            value = getattr(ns_domain, name)
        elif inp.solution_method == METHOD_FEMFEM_SP and hasattr(pf_domain, name):
            value = getattr(pf_domain, name)
        else:
            continue
        
        h5.write(value, '/%s' % name)
        
        # Save function names in a separate HDF attribute due to inability to 
        # list existing HDF groups when using the dolfin HDF5Function wrapper 
        assert ',' not in name
        funcnames.append(name)
    
    # Write HPC solution vectors
    if inp.solution_method == METHOD_FEMHPC_SP:
        h5.write(pf_domain.phi, '/hpc/phi')
        h5.write(pf_domain.phi_p, '/hpc/phi_p') 
    
    # Metadata
    tinfo = numpy.array([t, it, inp.dt])
    h5.write(tinfo, '/metadata/time_info')
    h5.attributes('/metadata')['restart_file_format'] = 1
    h5.attributes('/metadata')['functions'] = ','.join(funcnames)
    h5.attributes('/metadata')['input'] = str(inp)
    
    # Reports
    for rep_name, values in log.reports.items():
        values = numpy.array(values, dtype=float)
        h5.write(values, '/reports/%s' % rep_name)
    h5.attributes('/reports')['report_names'] = ','.join(log.reports)
    
    h5.close()


def merged_triangulation(pf_domain, ns_domain):
    """
    Create a triangulation of two merged domains
    """
    # Get combined triangulation
    comb_coords, comb_triangles = [], []
    domains = [ns_domain, pf_domain]
    for domain in domains:
        c, t = domain.get_triangulation()
        comb_coords.append(c)
        comb_triangles.append(t)
    Nc = sum(len(c) for c in comb_coords)
    
    # Get coordinates as one long numpy array,
    # and triangles as one long list
    coords = numpy.zeros((Nc, 2), float)
    triangles = []
    start = 0
    coord_map = {}
    coord_id_map = []
    for domain, c, t in zip(domains, comb_coords, comb_triangles):
        # Merge duplicated coordinates
        for x, y in c:
            key = (round(x, 6), round(y, 6))
            coord_id = coord_map.setdefault(key, len(coord_map))
            coord_id_map.append(coord_id)
            coords[coord_id] = x, y
        
        # Renumber triangle vertex numbers
        for v0, v1, v2 in t:
            vid0 = coord_id_map[v0+start]
            vid1 = coord_id_map[v1+start]
            vid2 = coord_id_map[v2+start]
            triangles.append((vid0, vid1, vid2))        
        
        start += len(c)
    coords = coords[:len(coord_map)]
    
    return coords, triangles, len(comb_triangles[0])


@contextlib.contextmanager
def parameter_setter(params, key, value):
    oldval = params[key] 
    params[key] = value
    yield
    params[key] = oldval


if __name__ == '__main__':
    inpfile = sys.argv[1]
    log = SimpleLog()
    inp = Input()
    inp.read(inpfile)
    log.info('Contents of input file %s:\n' % inpfile)
    log.dump_object(inp, '    ')
