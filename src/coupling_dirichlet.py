# encoding: utf-8
"""
Utility functions used by the FEMFEM and FEMHPC methods to couple the system matrices
for Dirichlet coupling and a shared plotting routine for showing the combined result
"""
from __future__ import division
import scipy.sparse
import numpy
from matplotlib import pyplot, tri
from utilities import COUPLED_DIRICHLET, COUPLED_NO


class DirichletCoupler(object):
    def __init__(self, inp, ns_domain, pf_domain):
        self.input = inp
        self.pf_domain = pf_domain
        self.ns_domain = ns_domain
        self.ns_u_map, self.pf_p_map = get_domain_coupling(ns_domain, pf_domain)
        self._cache = None
    
    def couple(self, linear_systems):
        phi_p, phi_pp = self.pf_domain.get_arrays()
        AA, bb, N1, cache = couple_matrices(linear_systems, self.input, phi_p, phi_pp,
                                            self.ns_u_map, self.pf_p_map, self.pf_domain,
                                            self._cache)
        self._cache = cache
        return AA, bb, N1
    
    def plot(self, simulation_time, stream_function, quiver):
        return plot_coupled(self.ns_domain, self.pf_domain, self.input,
                            simulation_time, stream_function, quiver)


def get_domain_coupling(ns_domain, pf_domain, geps=1e-14):
    """
    As a preprocessor we run through all lines separating the potential flow
    and the Navier-Stokes domains and get the mapping of dofs between the
    two so that we can apply Dirichlet BCs both ways in the time loop
    """
    ns_u_map = []
    pf_p_map = []
    
    for iline in range(ns_domain.num_dividing_lines):
        ns_dof_coords = ns_domain.get_dividing_line(iline)
        pf_dof_coords = pf_domain.get_dividing_line(iline)
        
        # Find the gradients in the potential flow domain to use as the 
        # Dirichlet boundary condition in the N-S domain
        ns_p_dof_coords = []
        pf_Ndl = len(pf_dof_coords)
        for ns_dof, ns_coord, ns_dir in ns_dof_coords:
            # Collect and skip pressure dofs
            if ns_dir == -1:
                ns_p_dof_coords.append((ns_dof, ns_coord))
                continue
            
            # Linear search of for matching potential flow
            for i in range(pf_Ndl-1):
                pf_dof0, pf_coord0 = pf_dof_coords[i]
                pf_dof1, pf_coord1 = pf_dof_coords[i+1]
                
                # Search until we find a match on the potential flow side
                match_x = pf_coord0[0] - geps < ns_coord[0] < pf_coord1[0]  + geps
                match_y = pf_coord0[1] - geps < ns_coord[1] < pf_coord1[1] + geps
                if match_x and match_y:
                    # Get the weights
                    dofs, weights = pf_domain.get_gradient_weights(ns_coord, pf_dof0, pf_dof1)
                    ns_u_map.append((ns_dof, dofs, weights[:,ns_dir]))
                    break
            else:
                raise IndexError('Did not find matching potential flow dof for x = %r!' % (ns_coord,))
        
        # Find the pressure dofs in the N-S domain to use in the Dirichlet boundary
        # conditions for the potential in the potential flow domain
        ns_Ndl = len(ns_p_dof_coords)
        for pf_dof, pf_coord in pf_dof_coords:
            # Linear search to find matching N-S pressure
            for i in range(ns_Ndl-1):
                ns_dof0, ns_coord0 = ns_p_dof_coords[i]
                ns_dof1, ns_coord1 = ns_p_dof_coords[i+1]
                
                # Search until we find a match on the potential flow side 
                match_x = ns_coord0[0] <= pf_coord[0] <= ns_coord1[0]
                match_y = ns_coord0[1] <= pf_coord[1] <= ns_coord1[1]  
                if match_x and match_y:
                    # Get the weights
                    dofs, weights = ns_domain.get_pressure_weights(pf_coord, ns_dof0, ns_dof1)
                    pf_p_map.append((pf_dof, dofs, weights))
                    break
            else:
                raise IndexError('Did not find matching pressure dof for x = %r!' % (pf_coord,))
    
    return ns_u_map, pf_p_map


def couple_matrices(linear_systems, inp, phi_p, phi_pp, ns_u_map, pf_p_map, pf_domain, cache):
    """
    Take the uncoupled matrices A1 (Navier-Stokes) and A2 (potential flow) and
    create a coupled system 
    
        AA = A1 C1    bb = b1
             C2 A2         b2
    
    The cache is used to hold the C1 and C2 matrices which are time invariant 
    """
    A1, A2, b1, b2 = linear_systems
    
    dt = inp.dt
    rho = inp.rho
    first_run = cache is None
    if first_run:
        cache = off_diagonal_blocks(A1, A2, ns_u_map, pf_p_map)
        tc0, tc1, tc2 = [1.0/dt, -1.0/dt, 0.0]
        phi_conv = phi_p
    else:
        tc0, tc1, tc2 = [1.5/dt, -2.0/dt, 0.5/dt]
        phi_conv = 2*phi_p - phi_pp
    C1, C2 = cache
    
    if inp.coupling_method == COUPLED_DIRICHLET:
        # Apply Dirichlet boundary conditions to the Navier-Stokes block matrix
        for ns_dof, _, _ in ns_u_map:
            apply_dirichlet(A1, ns_dof)
            b1[ns_dof] = 0
        
        # Apply Dirichlet boundary conditions to the potential flow block matrix
        # and update the right hand side vector with the non-linear term from the 
        # previous time step (see the off_diagonal_blocks() function).
        for pf_dof, _, _ in pf_p_map:
            # Coefficients to compute the derivative
            nbs, coeffs = pf_domain.velocity_dofs_and_weights(pf_dof)
            assert len(nbs) == len(coeffs)
            
            # Calculate explicit convective velocity
            nbs = numpy.asarray(nbs, int)
            phi_conv_nbs = phi_conv[nbs]
            uc, vc = numpy.dot(phi_conv_nbs, coeffs)
            
            # Update the A2 matrix with semi implicit Bernoulli convection term
            apply_dirichlet(A2, pf_dof, rho*tc0)
            for inb, nb in enumerate(nbs):
                # Velocity squared, semi implicit
                vel2 = coeffs[inb,0]*uc + coeffs[inb,1]*vc 
                A2[pf_dof,nb] += rho/2*vel2
            
            b2[pf_dof] = -rho*(phi_p[pf_dof]*tc1 + phi_pp[pf_dof]*tc2)
    
    else:
        assert inp.coupling_method == COUPLED_NO
        # Remove coupling
        if first_run:
            C1 *= 0
            C2 *= 0
    
    # Assemble the block matrix
    AA = scipy.sparse.bmat([[A1, C1], [C2, A2]], 'csr')
    N1, N2 = len(b1), len(b2)
    bb = numpy.zeros(N1 + N2, float)
    bb[:N1] = b1
    bb[N1:] = b2
    
    return AA, bb, N1, cache


def off_diagonal_blocks(A1, A2, ns_u_map, pf_p_map):
    """
    Return block matrices C1 and C2 which will be inserted as
    
        A1  C1
        C2  A2
    
    in the global matrix where A1 is the Navier-Stokes LHS and
    A2 is the potential flow LHS. C1 contains the derivatives
    of the potential used as Dirichlet BC for N-S velocity and
    C2 contains the N-S pressure to be used as Dirichlet BC for
    the potential (through the Bernoulli equation). 
    """
    C1 = scipy.sparse.lil_matrix((A1.shape[0], A2.shape[1]))
    C2 = scipy.sparse.lil_matrix((A2.shape[0], A1.shape[1]))
    
    # Dirichlet boundary conditions for the Navier-Stokes velocity
    for ns_dof, pf_dofs, pf_weights in ns_u_map:
        # u - ∇ϕ = 0
        for d, w in zip(pf_dofs, pf_weights):
            C1[ns_dof, d] += -w
    
    # Dirichlet boundary conditions for the potential using Bernulli's equation
    #    ∂ϕ/∂t + p/ρ + 1/2(∇ϕ)² + gy = C(t)
    # which gives, when pulling C(t) into ϕ and disregarding gravity: 
    #               ρ/Δt ϕ^{n+1} + p = - ρ/2(∇ϕ^n)² + ρ/Δt ϕ^n 
    # where we have used first order backward time differencing.
    for pf_dof, ns_p_dofs, ns_p_weights in pf_p_map:
        for d, w in zip(ns_p_dofs, ns_p_weights):
            C2[pf_dof, d] = w
    
    return C1.tocsr(), C2.tocsr()


def apply_dirichlet(A, row, diag_value=1):
    """
    Set row to be an identity row
        A[row,:] = 0
        A[row,row] = 1
    """
    j0, j1 = A.indptr[row], A.indptr[row+1]
    A.data[j0:j1] = 0
    A[row,row] = diag_value


def plot_coupled(ns_domain, pf_domain, inp, simulation_time, stream_function=None, quiver=False):
    """
    Plot the combined results in terms of velocity and pressure
    """
    # Get combined triangulation
    domains =  [ns_domain, pf_domain]
    comb_coords, comb_triangles = [], []
    for domain in domains:
        c, t = domain.get_triangulation()
        comb_coords.append(c)
        comb_triangles.append(t)
    Nc = sum(len(c) for c in comb_coords)
    
    # Get coordinates as one long numpy array,
    # triangles as one long list, and function
    # values as one long array from all domains
    coords = numpy.zeros((Nc, 2), float)
    triangles = []
    func_names = ['u0', 'u1', 'p']
    values = numpy.zeros((3, Nc), float)
    start = 0
    for domain, c, t in zip(domains, comb_coords, comb_triangles):
        n = len(c)
        coords[start:start+n] = c
        
        # Renumber triangle vertex numbers
        for v0, v1, v2 in t:
            triangles.append((v0+start, v1+start, v2+start))
        
        # Get function data
        for i, func_name in enumerate(func_names): 
            values[i,start:start+n] = domain.get_data(func_name)
        
        start += n
    
    # Combined triangulation
    X = numpy.array([c[0] for c in coords], float)
    Y = numpy.array([c[1] for c in coords], float)
    mesh = tri.Triangulation(X, Y, triangles)
    
    # Scaling
    scale_u = inp.U0
    scale_p = inp.U0**2*inp.rho
    U = values[0]/scale_u
    V = values[1]/scale_u
    
    # Get color bar limits
    maxabs_u = 2.5
    maxabs_p = abs(values[2]).max()/scale_p
    
    # Get figure and axes
    if hasattr(inp, '_plot_domains_save'):
        fig, axes = inp._plot_domains_save
        for ax in axes:
            ax.clear()
    else:
        fig = pyplot.figure(figsize=(12, 9))
        axes = [None]*6
        axes[0] = fig.add_axes([0.04, 0.75, 0.80, 0.25])
        axes[1] = fig.add_axes([0.04, 0.50, 0.80, 0.25])
        axes[2] = fig.add_axes([0.04, 0.25, 0.80, 0.25])
        axes[3] = fig.add_axes([0.04, 0.00, 0.80, 0.25])
        # Colorbar axes
        axes[4] = fig.add_axes([0.88, 0.55, 0.05, 0.35])
        axes[5] = fig.add_axes([0.88, 0.10, 0.05, 0.35])
        
        for ax in axes[:4]:
            ax.set_xlim(-inp.l1, inp.l2)
            ax.set_ylim(-inp.h2/2, inp.h2/2)
            ax.axis('equal')
    
    for ax in axes[:4]:
        ax.axis('off')
    
    # Setup color map to be blue via white to red with out of range colors cyan and pink
    cmap = pyplot.cm.get_cmap('RdBu_r')
    cmap.set_over('#ff7ee6')
    cmap.set_under('#25f4ff')
    cmap.set_bad('#acacac')
    params = dict(shading='gouraud', cmap=cmap)
    
    # Plot functions on triangulation
    Cu  = axes[0].tripcolor(mesh, values[0]/scale_u, vmin=-maxabs_u, vmax=maxabs_u, **params)
    _   = axes[1].tripcolor(mesh, values[1]/scale_u, vmin=-maxabs_u, vmax=maxabs_u, **params)
    Cp  = axes[3].tripcolor(mesh, values[2]/scale_p, vmin=-maxabs_p, vmax=maxabs_p, **params)
    
    # Stream function
    if stream_function:
        stream_function.compute()
        stream_function.plot(axes[2])
    
    # Quiver plot
    if quiver:
        params_quiver = dict(scale=inp.N2/2,
                             width=1/(inp.N2*4),
                             scale_units='x',
                             units='x')
        rs = numpy.random.RandomState()
        rs.seed(42)
        I = rs.rand(X.size) < 0.33
        axes[2].quiver(X[I], Y[I], U[I], V[I], **params_quiver)
    
    # Plot triangulation mesh lightly above the functions
    for ax in axes[2:3]:
        ax.triplot(mesh, c='#999999', lw=0.2)
    
    # Colorbars
    fig.colorbar(Cu, cax=axes[4])
    fig.colorbar(Cp, cax=axes[5])
    
    # Some informative text
    ax = axes[5]
    tp = dict(transform=fig.transFigure, family='monospace')
    text_propsR = dict(horizontalalignment='right', verticalalignment='bottom', **tp)
    text_propsC = dict(horizontalalignment='center', verticalalignment='center', **tp)
    ax.text(0.99, 0.04, 't=%5.2f' % simulation_time, **text_propsR)
    ax.text(0.99, 0.01, 'Re=%4g' % inp.Re, **text_propsR)
    ax.text(0.02, 0.875, 'u', **text_propsC)
    ax.text(0.02, 0.625, 'v', **text_propsC)
    ax.text(0.02, 0.125, 'p', **text_propsC)
    ax.text(0.905, 0.92, 'u, v', **text_propsC)
    ax.text(0.905, 0.47, 'p', **text_propsC)
    
    inp._plot_domains_save = fig, axes
    return fig
