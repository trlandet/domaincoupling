# encoding: utf-8
from __future__ import division
from matplotlib import pyplot
import dolfin as df
from utilities import merged_triangulation, METHOD_FEMFEM_DF
from subdomain_ns_fem import NavierStokesFEMSubDomain
from subdomain_pf_hpc import PotentialHPCSubDomain
from subdomains_nspf_fem import NavierStokesPotentialFlowFEMSubDomains


class FEMFEMDolfinMethod(object):
    def __init__(self, inp):
        """
        Method where we create one big mixed FEM function space for
        velocities, pressure and velocity potential in FEniCS  
        """
        assert inp.solution_method == METHOD_FEMFEM_DF
        self.input = inp
        
        # Get meshes for the two domains and merge them into one single triangulation
        # Note: we do not use the equation assembly routines in these two classes,
        # ONLY the mesh generation to get comparable results
        ns_domain = NavierStokesFEMSubDomain(inp)
        pf_domain = PotentialHPCSubDomain(inp)
        coords, triangles, N_triangs_ns = merged_triangulation(pf_domain, ns_domain)
        
        both_domains = NavierStokesPotentialFlowFEMSubDomains(inp, coords, triangles, N_triangs_ns)
        self.ns_domain = both_domains
        self.pf_domain = both_domains
        self.dx_ns = both_domains.dx_ns
    
    def assemble(self, t):
        return self.ns_domain.assemble(t)
    
    def couple(self, linear_systems):
        # No-op, the mixed space takes is already coupled at assembly time
        return linear_systems
    
    def solve(self, A, b):
        self.ns_domain.solve(A, b)
    
    def calculate_combined_functions(self):
        return self.ns_domain.calculate_combined_functions()
    
    def get_force(self):
        return self.ns_domain.get_force()
    
    def plot(self, simulation_time, stream_function=None, quiver=False):
        """
        Plot the combined velocity and pressure fields at the current time step
        """
        # Get figure and axes
        inp = self.input
        if hasattr(inp, '_plot_domains_save'):
            fig, axes = inp._plot_domains_save
            for ax in axes:
                ax.clear()
        else:
            fig = pyplot.figure(figsize=(12, 9))
            axes = [None]*6
            axes[0] = fig.add_axes([0.04, 0.75, 0.80, 0.25])
            axes[1] = fig.add_axes([0.04, 0.50, 0.80, 0.25])
            axes[2] = fig.add_axes([0.04, 0.25, 0.80, 0.25])
            axes[3] = fig.add_axes([0.04, 0.00, 0.80, 0.25])
            # Colorbar axes
            axes[4] = fig.add_axes([0.88, 0.55, 0.05, 0.35])
            axes[5] = fig.add_axes([0.88, 0.10, 0.05, 0.35])
            
        # Setup color map to be blue via white to red with out of range colors cyan and pink
        cmap = pyplot.cm.get_cmap('RdBu_r')
        cmap.set_over('#ff7ee6')
        cmap.set_under('#25f4ff')
        cmap.set_bad('#acacac')
        
        # Plot functions
        uC, pC = self.ns_domain.calculate_combined_functions()
        props = dict(backend='matplotlib', shading='gouraud')
        velprops = dict(backend='matplotlib', shading='gouraud',
                        vmin=-2.5*inp.U0, vmax=2.5*inp.U0)
        pyplot.sca(axes[0]); Cu0 = df.plot(uC[0], **velprops)
        pyplot.sca(axes[1]); Cu1 = df.plot(uC[1], **velprops)
        pyplot.sca(axes[3]); Cp = df.plot(pC, **props)
        
        # Stream function
        if stream_function:
            stream_function.compute()
            stream_function.plot(axes[2])
        
        # Quiver plot
        if quiver:
            pyplot.sca(axes[2])
            df.plot(uC, backend='matplotlib')
        
        # Plot triangulation mesh lightly above the functions
        mesh = uC[0].function_space().mesh()
        for ax in axes[2:3]:
            pyplot.sca(ax)
            df.plot(mesh, backend='matplotlib', c='#999999', lw=0.2)
        
        # Colorbars
        fig.colorbar(Cu0, cax=axes[4])
        fig.colorbar(Cp, cax=axes[5])
        
        for ax in axes[:4]:
            ax.axis('off')
        
        # Some informative text
        ax = axes[5]
        tp = dict(transform=fig.transFigure, family='monospace')
        text_propsR = dict(horizontalalignment='right', verticalalignment='bottom', **tp)
        text_propsC = dict(horizontalalignment='center', verticalalignment='center', **tp)
        ax.text(0.99, 0.04, 't=%5.2f' % simulation_time, **text_propsR)
        ax.text(0.99, 0.01, 'Re=%4g' % inp.Re, **text_propsR)
        ax.text(0.02, 0.875, 'u', **text_propsC)
        ax.text(0.02, 0.625, 'v', **text_propsC)
        ax.text(0.02, 0.125, 'p', **text_propsC)
        ax.text(0.905, 0.92, 'u, v', **text_propsC)
        ax.text(0.905, 0.47, 'p', **text_propsC)
        
        inp._plot_domains_save = fig, axes
        return fig
