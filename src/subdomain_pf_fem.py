# encoding: utf8
from __future__ import division
import numpy
import dolfin as df
from dolfin import dot, grad, dx
from utilities import parameter_setter, mat_to_csr, COUPLED_NO
from subdomain_pf_hpc import PotentialHPCSubDomain as _HPCImpl


class PotentialFEMSubDomain(object):
    def __init__(self, inp):
        self.input = inp
        
        self._create_mesh()
        self._create_functions()
        self._create_boundary_conditions()
        self._create_form()
        
        self._tensors = None, None
        self.is_first_time_step = True
        
    def _create_mesh(self):
        # Make sure we use the exact mesh as the HPC implementation 
        coords, triangles =_HPCImpl(self.input).get_triangulation()
        Nv = len(coords)
        Nc = len(triangles)
        
        # Generate the mesh
        mesh = self.mesh = df.Mesh()
        editor = df.MeshEditor()
        editor.open(mesh, 2, 2)
        editor.init_vertices(Nv)
        for i, c in enumerate(coords):
            editor.add_vertex(i, c[0], c[1])
        editor.init_cells(Nc)
        for i, t in enumerate(triangles):
            editor.add_cell(i, t[0], t[1], t[2])
        editor.close()
        
        # Mark the facets
        facet_marker = df.FacetFunction('size_t', mesh)
        def mark(marker, number, selector):
            class Region(df.SubDomain):
                def inside(self, x, on_boundary):
                    return selector(x, on_boundary)
            region = Region()
            region.mark(marker, number)
        facet_marker.set_all(0)
        eps = 1e-8
        x0, x1, _x2 = -self.input.l1, 0, self.input.l2
        y0, y1 = -self.input.h2/2, self.input.h2/2
        mark(facet_marker, 4, lambda x, ob: df.near(x[0], x1))                    # Coupling
        mark(facet_marker, 6, lambda x, ob: df.near(x[1], y1) and x[0] <= x1+eps) # PF top
        mark(facet_marker, 7, lambda x, ob: df.near(x[0], x0))                    # PF inlet
        mark(facet_marker, 8, lambda x, ob: df.near(x[1], y0) and x[0] <= x1+eps) # PF bottom
        
        self.mesh = mesh
        self.facet_marker = facet_marker
        self.ds = df.Measure('ds')(subdomain_data=facet_marker)
        self.vertex_coordinates = mesh.coordinates()
        
        # Connectivity from facet to cell
        self.mesh.init(1, 2)
        self.connectivity_FC = self.mesh.topology()(1, 2)
        
        # Connectivity from facet to vertex
        self.mesh.init(1, 0)
        self.connectivity_FV = self.mesh.topology()(1, 0)
    
    def _create_functions(self):
        params = df.parameters['form_compiler']
        with parameter_setter(params, 'no-evaluate_basis_derivatives', False):
            V = df.FunctionSpace(self.mesh, 'CG', self.input.Pu+1)
        
        self.phi = df.Function(V)
        self.phi_p = df.Function(V)
        self.phi_pp = df.Function(V)
        self.function_space = V
        self.dof_coordinates = V.tabulate_dof_coordinates().reshape((-1, 2))
        
        # Build connectivity dictionary from dof number to cell number
        self.connectivity_dof_cell = {}
        self.connectivity_cell_dof = {}
        dm = V.dofmap()
        for cell in df.cells(self.mesh):
            cid = cell.index()
            cdofs = dm.cell_dofs(cell.index())
            for dof in cdofs:
                self.connectivity_dof_cell.setdefault(dof, []).append(cid)
            self.connectivity_cell_dof[cid] = cdofs
            
        # Build connectivity from vertex to dof
        tmp = {tuple(x): dof for dof, x in enumerate(self.dof_coordinates)}
        self.connectivity_vertex_dof = {vid: tmp[tuple(x)] for vid, x in enumerate(self.vertex_coordinates)}
    
    def _create_boundary_conditions(self):        
        # This is changed every time step before assembly
        self.U0 = df.Constant(-1)
        
        # No Dirichlet BCs are applied in the coupled case
        # (these are handled on the SciPy / matrix side)
        V = self.function_space
        marker = self.facet_marker
        self.dirichlet_bcs = []
        
        self.coupled_boundaries = [4]
        self.potential_ds_boundaries = [6, 8]
        self.potential_inflow_boundaries = [7]
        
        if self.input.coupling_method == COUPLED_NO:
            # PF outlet BC
            self.dirichlet_bcs.append(df.DirichletBC(V, df.Constant(1), marker, 4))
    
    def _create_form(self):
        phi = df.TrialFunction(self.function_space)
        r = df.TestFunction(self.function_space)
        
        a = dot(grad(phi), grad(r))*dx
        L = 0
        
        # Walls
        zero = df.Constant(0)
        for region in self.potential_ds_boundaries:
            L -= zero*r*self.ds(region)
        
        # Inlet
        for region in self.potential_inflow_boundaries:
            L -= self.U0*r*self.ds(region)
        
        self._form_lhs = a
        self._form_rhs = L
    
    def get_dividing_line(self, line_number, only_dofs=True):
        """
        Get the dofs on the given line between the Navier-Stokes and potential 
        flow domains
        
          +---------------------------------
          |          
          |     +---line 1------------------
          |     |     
          |     |<--line 0
          |     |
          |     +---line 2------------------
          | 
          +---------------------------------
        
        The coordinate system is such that x = 0 on line 0 and y=+/- h2/2 on
        lines 1 and 2
        """
        assert line_number == 0
        
        if only_dofs:
            # Find dofs on the dividing line
            coords = self.dof_coordinates
            V = self.function_space
            
            dividing_line = []
            
            for dof in range(V.dim()):
                coord = coords[dof]
                if coord[0] > -1e-8:
                    dividing_line.append((dof, coord))
            
            dividing_line.sort(key=lambda item: item[1][0]+item[1][1])
            return dividing_line
        
        else:
            # Find facets and cells on the dividing line
            coords = self.mesh.coordinates()
            facet_and_cell_ids = []
            for facet in df.facets(self.mesh):
                if facet.midpoint().x() > -1e-8:
                    fid = facet.index()
                    cells = self.connectivity_FC(fid)
                    assert len(cells) == 1
                    vertices = self.connectivity_FV(fid)
                    assert len(vertices) == 2
                    i0, i1 = vertices
                    v0, v1 = tuple(coords[i0]), tuple(coords[i1])
                    if v0[0]+v0[1] > v1[0]+v1[1]:
                        v0, v1 = v1, v0
                    facet_and_cell_ids.append(((v0, v1), cells[0]))
            return facet_and_cell_ids
    
    def get_gradient_weights(self, coord, dof0, dof1):
        """
        Return the dofs and weights needed to calculate the gradient of phi at
        a given coordinate positioned somewhere on the line between the
        locations of dof0 and dof1.
        
        The dofs and weights for the x-derivative are dofs, coeffs[:,0] and
        the y-derivatives can be evaluated from dofs, coeffs[:,1].
        
        The method returns dofs, coeffs
        """
        # Find the cell these dofs have in common
        cells0 = self.connectivity_dof_cell[dof0]
        cells1 = self.connectivity_dof_cell[dof1]
        common_cells = [cid for cid in cells0 if cid in cells1]
        assert len(common_cells) == 1
        cid = common_cells[0]
        
        cell = df.Cell(self.mesh, cid)
        dofs, coeffs = self.velocity_dofs_and_weights(cell=cell, coord=coord)
        return dofs, coeffs
    
    def get_all_weights(self, coord, dof0=None, dof1=None, cell_idx=None):
        """
        Return the dofs and weights needed to calculate the phi, the gradient of
        phi and the double gradient of phi at a given coordinate positioned
        somewhere on the line between the locations of dof0 and dof1.
        
        This method returns the dofs and the coefficients where the i-th column
        of the coefficient matrix is as follows:
        
          - coeffs[:,0] = weights for ϕ
          - coeffs[:,1] = weights for ∂ϕ/∂x
          - coeffs[:,2] = weights for ∂ϕ/∂y 
          - coeffs[:,3] = weights for ∂²ϕ/∂x∂x
          - coeffs[:,4] = weights for ∂²ϕ/∂x∂y
          - coeffs[:,5] = weights for ∂²ϕ/∂y∂x
          - coeffs[:,6] = weights for ∂²ϕ/∂y∂y
        
        The first dimension of coeffs is the same length as the number of dofs
        """
        if cell_idx is None:
            # Find the cell that dof0 and dof1 have in common
            cells0 = self.connectivity_dof_cell[dof0]
            cells1 = self.connectivity_dof_cell[dof1]
            common_cells = [cid for cid in cells0 if cid in cells1]
            assert len(common_cells) == 1
            cid = common_cells[0]
        else:
            cid = cell_idx
            assert dof0 is None and dof1 is None
        
        cell = df.Cell(self.mesh, cid)
        el = self.function_space.element()
        coordinate_dofs = cell.get_vertex_coordinates()
        x = numpy.asarray(coord)
        
        Ndof = el.space_dimension()
        dofs = self.connectivity_cell_dof[cell.index()]
        assert len(dofs) == Ndof
        
        # Coefficients for phi
        coeffs0 = numpy.zeros((Ndof, 1), float)
        el.evaluate_basis_all(coeffs0, x, coordinate_dofs, cell.orientation())
        
        # Coefficients for grad(phi)
        coeffs1 = numpy.zeros((Ndof, 2), float)
        el.evaluate_basis_derivatives_all(1, coeffs1, x, coordinate_dofs, cell.orientation())
        
        # Coefficients for grad(grad(phi))
        coeffs2 = numpy.zeros((Ndof, 4), float)
        el.evaluate_basis_derivatives_all(2, coeffs2, x, coordinate_dofs, cell.orientation())
        
        # Assemble into a single coefficient matrix
        coeffs = numpy.zeros((Ndof, 7), float)
        coeffs[:,0:1] = coeffs0
        coeffs[:,1:3] = coeffs1
        coeffs[:,3:7] = coeffs2
        return dofs, coeffs
    
    def velocity_dofs_and_weights(self, dof=None, cell=None, coord=None):
        """
        Returns an array with N dof numbers and a NxD matrix of spatial
        derivative coefficients in the D spatial directions for each dof. This
        can be used to compute the spatial derivatives at the given coordinate
        from the values of the function at the given dofs
        
        You must either give a dof (from which a coordinate and a cell is
        found) or a cell and a coordinate 
        """
        if dof is not None:
            assert cell is None and coord is None, 'You must either give dof or cell and coord'
            cids = self.connectivity_dof_cell[dof]
            cells = [df.Cell(self.mesh, cid) for cid in cids]
            coord = self.dof_coordinates[dof]
        else:
            cells = [cell]
        
        el = self.function_space.element()
        coeffs = numpy.zeros((el.space_dimension(), 2), float)
        Nc = len(cells)
        
        all_dofs, all_coeffs = [], []
        for cell in cells:
            coordinate_dofs = cell.get_vertex_coordinates()
            dofs = self.connectivity_cell_dof[cell.index()]
            x = numpy.asarray(coord)
            el.evaluate_basis_derivatives_all(1, coeffs, x, coordinate_dofs, cell.orientation())
            for i, dof in enumerate(dofs):
                cdx, cdy = coeffs[i]
                all_dofs.append(dof)
                all_coeffs.append((cdx/Nc, cdy/Nc))
        
        return numpy.array(all_dofs, dtype=int), numpy.array(all_coeffs, dtype=float)
    
    def get_system(self, t):
        """
        Return linear system with normal BCs applied (not coupled)
        Matrix format is SciPy CSR
        """
        self.U0.assign(df.Constant(self.input.inlet_vel(t)))
        A, b = self._tensors
        
        # Assemble the bilinear form (time invariant)
        if A is None:
            A = df.assemble(self._form_lhs)
            for bc in self.dirichlet_bcs:
                bc.apply(A)
            A = mat_to_csr(A)
        else:
            self.is_first_time_step = False
        
        # Assemble linear form (time dependent)
        b = df.assemble(self._form_rhs, tensor=b)
        for bc in self.dirichlet_bcs:
            bc.apply(b)
        
        self._tensors = A, b
        return A, b.array()
    
    def explicit_velocity_at_dof(self, dof, prev=0):
        if prev == 0:
            vec = self.phi.vector()
        elif prev == 'conv':
            if self.is_first_time_step:
                vec = self.phi_p.vector().array()
            else:
                phi_p = self.phi_p.vector().array()
                phi_pp = self.phi_pp.vector().array()
                vec = 2*phi_p - phi_pp
        elif prev == 1:
            vec = self.phi_p.vector()
        else:
            assert prev == 2
            vec = self.phi_pp.vector()
        
        dofs, coeffs = self.velocity_dofs_and_weights(dof)
        u = v = 0
        for dof2, weights in zip(dofs, coeffs):
            phi = vec[dof2]
            u += weights[0]*phi
            v += weights[1]*phi
        return u, v
    
    def update(self, phi):
        self.phi_pp.assign(self.phi_p)
        self.phi_p.assign(self.phi)
        self.phi.vector().set_local(phi)
        self.phi.vector().apply('insert')
    
    def get_triangulation(self):
        triangles = []
        for cell in df.cells(self.mesh):
            cell_vertices = cell.entities(0)
            triangles.append(cell_vertices)
        return self.vertex_coordinates, triangles
    
    def get_arrays(self):
        phi = self.phi.vector().array()
        phi_p = self.phi_p.vector().array()
        return phi, phi_p
    
    def get_data(self, func_name):
        N = len(self.vertex_coordinates)
        values = numpy.zeros(N, float)
        rho = self.input.rho
        dt = self.input.dt
        phi = self.phi.compute_vertex_values()
        phi_p = self.phi_p.compute_vertex_values()
        phi_pp = self.phi_pp.compute_vertex_values()
        
        if self.is_first_time_step:
            tc0, tc1, tc2 = [1.0/dt, -1.0/dt, 0.0]
        else:
            tc0, tc1, tc2 = [1.5/dt, -2.0/dt, 0.5/dt]
        
        for vid in range(N):
            dof = self.connectivity_vertex_dof[vid]
            vel = self.explicit_velocity_at_dof(dof)
            if func_name == 'u0':
                values[vid] = vel[0]
            elif func_name == 'u1':
                values[vid] = vel[1]
            elif func_name == 'p':
                vel_c = self.explicit_velocity_at_dof(dof, 'conv')
                vel2 = vel[0]*vel_c[0] + vel[1]*vel_c[1]
                dphi_dt = tc0*phi[vid] + tc1*phi_p[vid] + tc2*phi_pp[vid]
                values[vid] = -vel2*rho/2 - dphi_dt*rho
        
        if func_name == 'p':
            values *= rho
        
        return values
