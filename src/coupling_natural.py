# encoding: utf-8
"""
Utility functions used by the FEMFEM and FEMHPC methods to couple the system matrices
for natural BC coupling
"""
from __future__ import division
import numpy
import scipy.sparse
from utilities import COUPLED_NATURAL, COUPLED_NATURAL_U_DIRICHLET_P, METHOD_FEMFEM_SP, METHOD_FEMHPC_SP
from coupling_dirichlet import plot_coupled, apply_dirichlet


class NaturalCoupler(object):
    def __init__(self, inp, ns_domain, pf_domain):
        self.input = inp
        self.pf_domain = pf_domain
        self.ns_domain = ns_domain
        self._cache = None
        
        fem_pot = inp.solution_method == METHOD_FEMFEM_SP
        dirichlet_pressure = inp.coupling_method == COUPLED_NATURAL_U_DIRICHLET_P
        self.coupling_info = get_domain_coupling_natural(ns_domain, pf_domain, fem_pot, dirichlet_pressure)
    
    def couple(self, linear_systems):
        phi_p, phi_pp = self.pf_domain.get_arrays()
        AA, bb, N1, cache = couple_matrices_natural(linear_systems, self.input,
                                                    self.ns_domain, self.coupling_info,
                                                    phi_p, phi_pp, self._cache)
        self._cache = cache
        return AA, bb, N1
    
    def plot(self, simulation_time, stream_function, quiver):
        return plot_coupled(self.ns_domain, self.pf_domain, self.input,
                            simulation_time, stream_function, quiver)


# Construct table of Gauss-Legendre quadrature rules in 1D
GL_QUADRATURE_TABLE = {}
for n in range(1, 21):
    points, weights = numpy.polynomial.legendre.leggauss(n)
    GL_QUADRATURE_TABLE[n] = (points, weights)


# Indices into the potential flow weights array
WT_PHI, WT_PHI_DX, WT_PHI_DY = 0, 1, 2
WT_PHI_DXDX, WT_PHI_DXDY, _WT_PHI_DYDX, _WT_PHI_DYDY = 3, 4, 5, 6


class CouplingPoint(object):
    def __init__(self, quadrature_weight=None, phi_dof=None):
        self.quadrature_weight = quadrature_weight
        self.phi_dof = phi_dof
        
        self.u_weights = None
        self.u0_dofs = None
        self.u1_dofs = None
        self.p_weights = None
        self.p_dofs = None
        
        self.phi_weights = None
        self.phi_dofs = None
        self.grad_phi_weights = None
        self.grad_phi_dofs = None
    
    def set_ns_dofs_and_weights(self, ns_domain, ns_cell_idx, coord):
        """
        Get and store the information necessary to find the velocity and
        pressure at the given coordinate in the given cell from a solution
        vector containing the velocity and pressure for all dofs
        """
        self.u0_dofs, self.u1_dofs, self.u_weights = ns_domain.get_velocity_weights(coord, ns_cell_idx)
        self.p_dofs, self.p_weights = ns_domain.get_pressure_weights(coord, cell_idx=ns_cell_idx)
        
    def set_pf_dofs_and_all_weights(self, pf_domain, coord, pf_dof0=None, pf_dof1=None, cell_idx=None):
        """
        Get and store the information necessary to find the potential and
        various derivatives of the potential at the given coordinate between
        the locations of the given dofs from a solution vector containing the
        velocity and pressure for all dofs
        """
        if cell_idx is None:
            self.phi_dofs, self.phi_weights = pf_domain.get_all_weights(coord, pf_dof0, pf_dof1)
        else:
            assert pf_dof0 is None and pf_dof1 is None
            self.phi_dofs, self.phi_weights = pf_domain.get_all_weights(coord, cell_idx=cell_idx)
    
    def set_pf_dofs_and_vel_weights(self, pf_domain, pf_dof):
        """
        Get and store the information necessary to find the potential and
        various derivatives of the potential at the given coordinate between
        the locations of the given dofs from a solution vector containing the
        velocity and pressure for all dofs
        """
        self.grad_phi_dofs, self.grad_phi_weights = pf_domain.velocity_dofs_and_weights(pf_dof)


def get_domain_coupling_natural(ns_domain, pf_domain, fem_pot=False, dirichlet_pressure=False, geps=1e-14):
    """
    As a preprocessor we run through all lines separating the potential flow
    and the Navier-Stokes domains and get the mapping of dofs between the
    two so that we can apply natural BCs both ways in the time loop
    """
    ns_side_info = []
    pf_side_info = []
    
    # Find information required to perform quadrature of the facet integrals on the
    # coupled boundary in the weak form of the Navier-Stokes equations
    for iline in range(ns_domain.num_dividing_lines):
        ns_facets_and_cells = ns_domain.get_dividing_line(iline, only_dofs=False)
        pf_dof_coords = pf_domain.get_dividing_line(iline)
        
        ns_info = domain_coupling_C1_info(ns_domain, pf_domain, ns_facets_and_cells, pf_dof_coords, geps)
        ns_side_info.extend(ns_info)
        
        if dirichlet_pressure or not fem_pot:
            pf_info = domain_coupling_C2_info_hpc(ns_domain, pf_domain, ns_facets_and_cells, pf_dof_coords, geps)
        else:
            pf_info = domain_coupling_C2_info_fem(ns_domain, pf_domain, ns_facets_and_cells, iline, geps)
        
        pf_side_info.extend(pf_info)
    
    # Construct dofmap from coupled functions to segregated functions
    # Needed to be able to use matrix (coupled) indices to look up values
    # in the u_convX vectors
    gdim = ns_domain.mesh.geometry().dim()
    V = ns_domain.u_conv0.function_space()
    W = ns_domain.funcspace
    coords_coupled = W.tabulate_dof_coordinates().reshape((-1, gdim))
    coords_split = V.tabulate_dof_coordinates().reshape((-1, gdim))
    coupled_to_split_mapping = [-1]*coords_coupled.shape[0]
    for i in range(gdim):
        dofs = W.sub(i).dofmap().dofs()
        coord_to_dof = {tuple(coords_coupled[d]): d for d in dofs}
        for sdof, coord in enumerate(coords_split):
            cdof = coord_to_dof[tuple(coord)]
            coupled_to_split_mapping[cdof] = sdof
    
    return ns_side_info, pf_side_info, coupled_to_split_mapping


def domain_coupling_C1_info(ns_domain, pf_domain, ns_facets_and_cells, pf_dof_coords, geps=1e-14):
    """
    Gather information needed to assemble the C1 matrix
    """
    # Max needed polynomial degree
    P = 8
    # Get quadrature degree, points and weights
    Q = int(numpy.ceil((P + 1) / 2.0))
    gl_points, gl_weights = GL_QUADRATURE_TABLE[Q]
    
    # Loop over all N-S facets to find dofs and weights needed to perform ds boundary integrals
    pf_Ndl = len(pf_dof_coords)
    ns_u_points = []
    for ns_facet_coords, ns_cell_idx in ns_facets_and_cells:
        for pt, wt in zip(gl_points, gl_weights):
            # Transform from reference quadrature rule coordinate pt ∈ [-1, 1] to physical x and y
            fac = (1 + pt)/2
            x0, y0 = ns_facet_coords[0]
            x1, y1 = ns_facet_coords[1]
            x = x0*fac + x1*(1-fac)
            y = y0*fac + y1*(1-fac)
            ns_coord = (x, y)
            
            # Scale the quadrature weight according to the facet and reference element lengths (Jacobian)
            facet_length = ((x1 - x0)**2 + (y0 - y1)**2)**0.5
            wt_scale = wt*facet_length/2
            
            # Linear search of for matching potential flow
            for i in range(pf_Ndl-1):
                pf_dof0, pf_coord0 = pf_dof_coords[i]
                pf_dof1, pf_coord1 = pf_dof_coords[i+1]
                
                # Search until we find a match on the potential flow side
                match_x = pf_coord0[0] - geps < x < pf_coord1[0] + geps
                match_y = pf_coord0[1] - geps < y < pf_coord1[1] + geps
                if match_x and match_y:
                    cp = CouplingPoint(wt_scale)
                    cp.set_ns_dofs_and_weights(ns_domain, ns_cell_idx, ns_coord)
                    cp.set_pf_dofs_and_all_weights(pf_domain, ns_coord, pf_dof0, pf_dof1)
                    ns_u_points.append(cp)
                    break
            else:
                raise IndexError('Did not find matching potential flow dof for x = %r!' % (ns_coord,))
    
    return ns_u_points


def domain_coupling_C2_info_fem(ns_domain, pf_domain, ns_facets_and_cells, iline, geps=1e-14):
    """
    Gather information needed to assemble the C2 matrix when the PF side is FEM
    and the potential is coupled via natural BCs
    """
    # Max needed polynomial degree
    P = 6
    # Get quadrature degree, points and weights
    Q = int(numpy.ceil((P + 1) / 2.0))
    gl_points, gl_weights = GL_QUADRATURE_TABLE[Q]
    
    pf_facets_and_cells = pf_domain.get_dividing_line(iline, only_dofs=False)
            
    # Loop over all PF facets to find dofs and weights needed to perform ds boundary integral
    pf_phi_points = []
    for pf_facet_coords, pf_cell_idx in pf_facets_and_cells:
        for pt, wt in zip(gl_points, gl_weights):
            # Transform from reference quadrature rule coordinate pt ∈ [-1, 1] to physical x and y
            fac = (1 + pt)/2
            x0, y0 = pf_facet_coords[0]
            x1, y1 = pf_facet_coords[1]
            x = x0*fac + x1*(1-fac)
            y = y0*fac + y1*(1-fac)
            pf_coord = (x, y)
            
            # Scale the quadrature weight according to the facet and reference element lengths (Jacobian)
            facet_length = ((x1 - x0)**2 + (y0 - y1)**2)**0.5
            wt_scale = wt*facet_length/2
            
            # Search to find matching N-S velocity dofs
            for ns_facet_coords, ns_cell_idx in ns_facets_and_cells:
                ns_coord0, ns_coord1 = ns_facet_coords
                match_x = ns_coord0[0] - geps  <= pf_coord[0] <= ns_coord1[0] + geps 
                match_y = ns_coord0[1] - geps  <= pf_coord[1] <= ns_coord1[1] + geps 
                if match_x and match_y:
                    cp = CouplingPoint(wt_scale)
                    cp.set_ns_dofs_and_weights(ns_domain, ns_cell_idx, pf_coord)
                    cp.set_pf_dofs_and_all_weights(pf_domain, pf_coord, cell_idx=pf_cell_idx)
                    pf_phi_points.append(cp)
                    break
            else:
                raise IndexError('Did not find matching velocity dof for x = %r!' % (pf_coord,))
    return pf_phi_points


def domain_coupling_C2_info_hpc(ns_domain, pf_domain, ns_facets_and_cells, pf_dof_coords, geps=1e-14):
    """
    Gather information needed to assemble the C2 matrix when the PF side is HPC
    or the potential is coupled to the pressure via Dirichlet BCs
    """        
    # Find the velocity in the N-S domain at the same location as the coupled PF dofs
    pf_phi_points = []
    for pf_dof, pf_coord in pf_dof_coords:
        # Search to find matching N-S velocity dofs
        for ns_facet_coords, ns_cell_idx in ns_facets_and_cells:
            ns_coord0, ns_coord1 = ns_facet_coords
            match_x = ns_coord0[0] - geps  <= pf_coord[0] <= ns_coord1[0] + geps 
            match_y = ns_coord0[1] - geps  <= pf_coord[1] <= ns_coord1[1] + geps   
            if match_x and match_y:
                cp = CouplingPoint(phi_dof=pf_dof)
                cp.set_ns_dofs_and_weights(ns_domain, ns_cell_idx, pf_coord)
                cp.set_pf_dofs_and_vel_weights(pf_domain, pf_dof)
                pf_phi_points.append(cp)
                break
        else:
            raise IndexError('Did not find matching velocity dof for x = %r!' % (pf_coord,))
    
    return pf_phi_points


def couple_matrices_natural(linear_systems, inp, ns_domain, coupling_info, phi_p, phi_pp, cache):
    """
    Take the uncoupled matrices A1 (Navier-Stokes) and A2 (potential flow) and
    create a coupled system 
    
        AA = A1 C1    bb = b1
             C2 A2         b2
    
    The cache is used to hold the C1 and C2 matrices which are time invariant 
    """
    assert inp.coupling_method in (COUPLED_NATURAL, COUPLED_NATURAL_U_DIRICHLET_P)
    A1, A2, b1, b2 = linear_systems
    ns_u_points, pf_phi_points, coupled_to_split_mapping = coupling_info
    
    dt = inp.dt
    rho = inp.rho
    mu = inp.mu
    u_conv0 = ns_domain.u_conv0.vector().array()
    u_conv1 = ns_domain.u_conv1.vector().array()
    first_run = cache is None
    natural_pressure_bc = inp.coupling_method != COUPLED_NATURAL_U_DIRICHLET_P
    
    if first_run:
        it = 1
        tc0, tc1, tc2 = [1.0/dt, -1.0/dt, 0.0]
        phi_conv = phi_p
        
    else:
        it = cache[0] + 1
        tc0, tc1, tc2 = [1.5/dt, -2.0/dt, 0.5/dt]
        phi_conv = 2*phi_p - phi_pp
    
    ###########################################################################
    # The C1 matrix
    if it in (1, 2):
        # C1 only depends on time only through the time coefficient tc0
        C1 = scipy.sparse.lil_matrix((A1.shape[0], A2.shape[1]))
        
        # FIXME: assumes n = [-1, 0]
        for pt in ns_u_points:
            # Loop over test functions v and trial functions phi
            for iv, wtu in enumerate(pt.u_weights):
                dof_v0 = pt.u0_dofs[iv]
                dof_v1 = pt.u1_dofs[iv]
                for ip, dof_phi in enumerate(pt.phi_dofs):
                    # -<μ ∇(∇ϕ)⋅n, v>
                    d0 = mu*pt.phi_weights[ip,WT_PHI_DXDX]*wtu
                    d1 = mu*pt.phi_weights[ip,WT_PHI_DXDY]*wtu
                    
                    if natural_pressure_bc:
                        # -<ρ ∂ϕ/∂t, v⋅n> = -<ρ (γ0 ϕ + γ1 ϕp + γ2 ϕpp), v⋅n>
                        d0 += rho*tc0*pt.phi_weights[ip,WT_PHI]*wtu
                    
                    # Insert into the coupling matrix
                    C1[dof_v0, dof_phi] += pt.quadrature_weight*d0
                    C1[dof_v1, dof_phi] += pt.quadrature_weight*d1
        
        C1 = C1#.tocsr()
    else:
        C1 = cache[1]
    
    ###########################################################################
    # The C2 matrix
        
    if it == 1:
        # C2 is time invariant
        C2 = scipy.sparse.lil_matrix((A2.shape[0], A1.shape[1]))
        
        if inp.coupling_method == COUPLED_NATURAL_U_DIRICHLET_P:
            # Dirichlet boundary conditions for the potential using Bernulli's equation
            #    ∂ϕ/∂t + p/ρ + 1/2(∇ϕ)² + gy = C(t)
            # which gives, when pulling C(t) into ϕ and disregarding gravity: 
            #    ρ γ0 ϕ + p = - ρ/2 ∇ϕc⋅∇ϕ - ρ (γ1 ϕp + γ2 ϕpp) 
            # where we have used first order backward time differencing.
            for pt in pf_phi_points:
                for ns_p_dof, ns_p_weight in zip(pt.p_dofs, pt.p_weights):
                    C2[pt.phi_dof, ns_p_dof] = ns_p_weight
        
        elif inp.solution_method == METHOD_FEMHPC_SP:
            # FIXME: assumes n = [1, 0]
            for pt in pf_phi_points:
                for ns_u_dof, ns_u_coeff in zip(pt.u0_dofs, pt.u_weights):
                    # ∇ϕ - u = 0
                    C2[pt.phi_dof, ns_u_dof] = -ns_u_coeff
        
        elif inp.solution_method == METHOD_FEMFEM_SP:
            # FIXME: assumes n = [1, 0]
            for pt in pf_phi_points:
                # Loop over test functions r and trial functions u
                for ir, wtphi in enumerate(pt.phi_weights[:,WT_PHI]):
                    dof_r = pt.phi_dofs[ir]
                    for iu, wtu in enumerate(pt.u_weights):
                        dof_u0 = pt.u0_dofs[iu]
                        #dof_u1 = cp.u1_dofs[iu]
                        
                        # -<u⋅n, r>
                        d0 = -wtu*wtphi
                        
                        # Insert into the coupling matrix
                        C2[dof_r, dof_u0] += pt.quadrature_weight*d0
        
        else:
            raise NotImplementedError('Method %r is not implemented' % inp.solution_method)
                
        C2 = C2.tocsr()
    else:
        C2 = cache[2]
    
    ###########################################################################
    # The C1c matrix (C1 + convective terms)
    # The linearized convective terms needs to be assembled every iteration 
    C1c = C1.copy()
    
    # FIXME: assumes n = [-1, 0]
    for pt in ns_u_points:
        dofs_u0s = [coupled_to_split_mapping[d] for d in pt.u0_dofs]
        dofs_u1s = [coupled_to_split_mapping[d] for d in pt.u1_dofs]
        uc0 = numpy.dot(pt.u_weights, u_conv0[dofs_u0s])
        uc1 = numpy.dot(pt.u_weights, u_conv1[dofs_u1s])
        grad_phi_c0 = numpy.dot(pt.phi_weights[:,WT_PHI_DX], phi_conv[pt.phi_dofs])
        grad_phi_c1 = numpy.dot(pt.phi_weights[:,WT_PHI_DY], phi_conv[pt.phi_dofs])
        
        # Loop over test functions v and trial functions phi
        for iv, wtu in enumerate(pt.u_weights):
            dof_v0 = pt.u0_dofs[iv]
            dof_v1 = pt.u1_dofs[iv]
            for ip, dof_phi in enumerate(pt.phi_dofs):
                # +<ρ uc⋅n ∇ϕ, v>
                d0 = -rho*uc0*pt.phi_weights[ip,WT_PHI_DX]*wtu
                d1 = -rho*uc0*pt.phi_weights[ip,WT_PHI_DY]*wtu
                
                if natural_pressure_bc:
                    if True:
                        # -<ρ/2 ∇ϕ⋅∇ϕc, v⋅n>
                        vel2 = grad_phi_c0*pt.phi_weights[ip,WT_PHI_DX] + grad_phi_c1*pt.phi_weights[ip,WT_PHI_DY]
                        d0 += rho/2*vel2*wtu
                    else:
                        # -<ρ/2 uc⋅uc, v⋅n>
                        b1[dof_v0] -= rho/2*(uc0**2 + uc1**2)*wtu*pt.quadrature_weight 
                
                # Insert into the coupling matrix
                C1c[dof_v0, dof_phi] += pt.quadrature_weight*d0
                C1c[dof_v1, dof_phi] += pt.quadrature_weight*d1
    
    ###########################################################################
    # Assemble RHS vector
    # FIXME: assumes n = [-1, 0]
    if natural_pressure_bc:
        #for wt, dofs_u0, dofs_u1, weights_u, dofs_phi, weights_phi, _, _ in facet_integral_quadr_info_ns:
        for pt in ns_u_points:
            pp = numpy.dot(pt.phi_weights[:,WT_PHI], phi_p[pt.phi_dofs])
            ppp = numpy.dot(pt.phi_weights[:,WT_PHI], phi_pp[pt.phi_dofs])
            for iv, wtu in enumerate(pt.u_weights):
                dof_v0 = pt.u0_dofs[iv]
                # -<ρ ∂ϕ/∂t, v⋅n> = -<ρ (γ0 ϕ + γ1 ϕp + γ2 ϕpp), v⋅n>
                b1[dof_v0] -= pt.quadrature_weight*rho*(tc1*pp + tc2*ppp)*wtu
    
    ###########################################################################
    # Apply Dirichlet boundary conditions to the potential flow block matrix
    # and update the right hand side vector with the non-linear term from the 
    # previous time step (see the off_diagonal_blocks() function).
    if inp.coupling_method == COUPLED_NATURAL_U_DIRICHLET_P:
        #for pf_dof, _, _, grad_phi_dofs, grad_phi_coeffs in pf_p_map:
        for pt in pf_phi_points: 
            # Calculate explicit convective velocity
            phi_conv_vals = [phi_conv[d] for d in pt.grad_phi_dofs]
            uc, vc = numpy.dot(phi_conv_vals, pt.grad_phi_weights)
            
            # Update the A2 matrix with semi implicit Bernoulli convection term
            #      ρ γ0 ϕ + p = - ρ/2(∇ϕ^n)² - ρ (γ1 ϕp + γ2 ϕpp)
            apply_dirichlet(A2, pt.phi_dof, rho*tc0)
            for inb, nb in enumerate(pt.grad_phi_dofs):
                # Velocity squared, semi implicit
                c0, c1 = pt.grad_phi_weights[inb]
                vel2 = c0*uc + c1*vc 
                A2[pt.phi_dof, nb] += rho/2*vel2
            
            b2[pt.phi_dof] = -rho*(phi_p[pt.phi_dof]*tc1 + phi_pp[pt.phi_dof]*tc2)
    
    if not natural_pressure_bc:
        for pt in ns_u_points:
            # Loop over test functions v and trial functions phi
            for iv, wtu in enumerate(pt.u_weights):
                dof_v0 = pt.u0_dofs[iv]
                for ip, dof_p in enumerate(pt.p_dofs):
                    # <p, v⋅n>
                    A1[dof_v0, dof_p] -= pt.quadrature_weight*wtu*pt.p_weights[ip]
    
    ###########################################################################
    # Assemble the block matrix
    AA = scipy.sparse.bmat([[A1, C1c], [C2, A2]], 'csr')
    N1, N2 = len(b1), len(b2)
    bb = numpy.zeros(N1 + N2, float)
    bb[:N1] = b1
    bb[N1:] = b2
    
    # Store the cache for later iterations
    cache = it, C1, C2
        
    return AA, bb, N1, cache
