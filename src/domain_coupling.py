# encoding: utf8
"""
Flow around a cylinder by domain decomposition where a simple FEM
Navier-Stokes solver written with FEniCS is coupled to a potential
flow Laplace solver written with FEM or HPC methods.

Example computational domain:

  +-----------------+-----------------------------
  |    Pot.flow     |    Navier-Stokes
  | -->             |    
  | -->             |  /--\
  | -->             |  |  |   <-- cylinder
  | -->             |  \--/ 
  | -->             |
  +-----------------+------------------------------

We couple the Navier-Stokes velocity and the potential flow velocity
potential by u = grad(phi) with either Dirichlet or natural boundary
conditions. The Navier-Stokes pressure is coupled with the potential
through the Bernoulli and this is also done either with Dirichlet or
natural boundary conditions.
"""
from __future__ import division
import time
import matplotlib; matplotlib.use('Agg')
import dolfin as df
from utilities import Input, SimpleLog, StreamFunction, SolutionProperties, save_simulation
from method_femfem_dolfin import FEMFEMDolfinMethod
from method_femfem_scipy import FEMFEMScipyMethod
from method_femhpc_scipy import FEMHPCScipyMethod


def get_numerical_method(inp):
    """
    Return a domain for the given input numerical method
    """
    method = inp.solution_method
    
    if method == 'femfem_dolfin':
        return FEMFEMDolfinMethod(inp)
    elif method == 'femfem_scipy':
        return FEMFEMScipyMethod(inp)
    elif method == 'femhpc_scipy':
        return FEMHPCScipyMethod(inp)
    else:
        raise NotImplementedError('The numerical method %s is not implemented' % method)


def main(inp):
    log = SimpleLog('%scoupling.log' % inp.output_prefix)
    log.info('Running domain coupling with input:\n')
    log.dump_object(inp, '    ')
    
    # Get the numerical method 
    method = get_numerical_method(inp)
    uC, _uP = method.calculate_combined_functions()
    stream_function = StreamFunction(uC)
    solprops = SolutionProperties(method.ns_domain.u, dt=inp.dt, nu=inp.mu/inp.rho, dx=method.dx_ns)
    
    # Time loop
    t = 0
    it = 0
    dt = inp.dt
    timer_loop_start = time.time()
    while t <= inp.tmax + 1e-6 - dt:
        t += dt
        it += 1
        timer_ts_start = time.time()
        log.report('Timestep', it, '%5d')
        log.report('t', t, '%8.4f')
        
        # Assemble the two system matrices
        with log.timer('Assemble'):
            linear_systems = method.assemble(t)
        
        # Couple the system matrices into one block matrix
        with log.timer('Couple'):    
            A, b = method.couple(linear_systems)
        
        # Solve the block matrix system
        with log.timer('Solve'): 
            method.solve(A, b)
            
        # Calculate forces on the cylinder and the Courant and Peclet numbers 
        Fv, Fp = method.get_force()
        Co_max = solprops.courant_number().vector().max()
        Pe_max = solprops.peclet_number().vector().max()
        success = Co_max < 1
        
        # Calculate the difference in NS velocities from the previous time step
        du0 = df.errornorm(method.ns_domain.u0_p, method.ns_domain.u0, degree_rise=0)
        du1 = df.errornorm(method.ns_domain.u1_p, method.ns_domain.u1, degree_rise=0)
        
        with log.timer('Plot'):
            if it % inp.output_step == 0 or not success:
                fig = method.plot(t, stream_function)
                fig.savefig('%stimestep_%05d_t_%08d.png' % (inp.output_prefix, it, round(t*1e4)), dpi=100)
                log.flush()
        
        # Report timestep values
        log.report('TsTime', time.time() - timer_ts_start, '%4.2fs')
        log.report('Fp0', Fp[0])
        log.report('Fp1', Fp[1])
        log.report('Fv0', Fv[0])
        log.report('Fv1', Fv[1])
        log.report('Co', Co_max, '%6.1e')
        log.report('Pe', Pe_max, '%6.1e')
        log.report('du', du0 + du1, '%6.1e')
        log.report_timestep()
        
        if it % inp.output_step_fulldump == 0:
            h5_file_name = '%srestart.h5' % inp.output_prefix
            save_simulation(method, h5_file_name, t, it, inp, log)
        elif not success:
            h5_file_name = '%sdiverged.h5' % inp.output_prefix
            save_simulation(method, h5_file_name, t, it, inp, log)
        
        if not success:
            log.error('Courant number > 1\n')
            break
    
    log.info('DONE in %.2fs\n' % (time.time() - timer_loop_start))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', nargs='?', default=None)
    parser.add_argument('-N', type=int, default=0,
                        help='number of elements over the height')
    parser.add_argument('--tmax', type=float, default=0,
                        help='simulation time')
    parser.add_argument('--dt', type=float, default=0,
                        help='simulation time step')
    parser.add_argument('-s', '--output-step', type=int, default=0,
                        help='timesteps between each generated plot')
    args = parser.parse_args()
    
    inp = Input()
    if args.input_file:
        inp.read(args.input_file)
    
    if args.N:
        inp.N1 = inp.N2 = args.N
    if args.tmax:
        inp.tmax = args.tmax
    if args.dt:
        inp.dt = args.dt
    if args.output_step:
        inp.output_step = args.output_step
    
    main(inp)
