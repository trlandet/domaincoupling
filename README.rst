Domain coupling
===============

This repository contains code to test various ways to couple potential flow and
Navier-Stokes flow solvers. The code requires FEniCS and the HPC code from
https://bitbucket.org/trlandet/hpc.

The program to run is ``domain_coupling.py``. The program will get the solution
method selected in the provided input file and start solving the given problem
(flow around various bodies in 2D mostly).

Implemented solution methods:

- femfem_dolfin: one big mixed FEM function space of u, p and phi
- femfem_scipy: two FEM matrix blocks (Navier-Stokes and Laplace) coupled 
  "manually" by use of SciPy
- femhpc_scipy: one FEM and HPC matrix coupled "manually" by use of SciPy

The methods support various forms of coupling the Navier-Stokes and the Laplace
equation: through Dirichlet boundary conditions (weakly/strongly) and through
natural boundary conditions.  

All code (c) Tormod Landet 2016
